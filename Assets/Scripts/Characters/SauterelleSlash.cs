﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SauterelleSlash : MonoBehaviour
{
    public Character creator;
    public int damage;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject potato = collision.gameObject;

        if (potato != creator.gameObject)
        {
            if (potato.tag == "player")
            {
                potato.GetComponent<Character>().Damage(damage, creator.playerNum);
            }
        }
    }
}
