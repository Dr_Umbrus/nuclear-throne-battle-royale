﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sauterelle : Character
{
    public GameObject slash;
    public GameObject omniSlash;
    public List<Vector3> slashPos;
    List<int> intPos= new List<int>();
    //La direction de saut
    Vector2 target;

 
    //La vitese du saut
    public float speedDash;

    //Si on est en train de sauter
    bool jumping=false;

    //Un timer
    float move = 0;

    public BossMantis mantis;
    public List<Character> victimes=new List<Character>();

    public override void Starting()
    {
        GameObject[] slashes = GameObject.FindGameObjectsWithTag("slash");
        foreach(GameObject sla in slashes)
        {
            slashPos.Add(sla.transform.position);
        }
    }

    public override void Competence()
    {
        mantis = null;
        victimes.Clear();
        //On prend la direction dans laquelle le joueur était en train de se déplacer
        target = movement;
        //Le vecteur est normalisé
        target.Normalize();

        //Le joueur ne peut pas se déplacer pendant le saut
        stopped = true;

        //On passe en mode saut.
        jumping = true;

        //Le timer est reset
        move = 0;
        slash.SetActive(true);
        anim.SetTrigger("action");
    }

    public override void Pattern()
    {

        if (Input.GetKeyDown(KeyCode.H))
        {
            Ultimate();
        }
        //Le timer est en route
        move += Time.deltaTime;

        //Pendant le saut
        if (jumping)
        {

            transform.Translate(target * speedDash*Time.deltaTime/10);

            //Au bout d'une seconde, le jump s'arrête
            if (move >= 1)
            {
                anim.SetTrigger("endaction");
                jumping = false;
                if (life > 0)
                {
                    stopped = false;
                }
                
                slash.SetActive(false);
            }
        }

        

    }

    public override void Ultimate()
    {
        StartCoroutine(UltiSlash());   
    }

    IEnumerator UltiSlash()
    {
        for (int i = 0; i < slashPos.Count; i++)
        {
            intPos.Add(i);
        }
            Debug.Log("potato");
        box.enabled = false;
        sr.color = Color.clear;
        stopped = true;
        for (int i = 0; i < 15; i++)
        {
            int posi = Random.Range(0, intPos.Count);
            

            GameObject temp=Instantiate(omniSlash, transform.position, Quaternion.identity);
            temp.GetComponent<OmniSlash>().SetStats(gm.nPlayer, playerNum, slashPos[intPos[posi]]);
            intPos.RemoveAt(posi);

            yield return new WaitForSeconds(0.4f);
        }

        box.enabled = true;
        sr.color = Color.white;
        yield return new WaitForSeconds(0.75f);
        stopped = false;

    }
}
