﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moth : Character
{

    public GameObject burst;


    public override void Competence()
    {
        anim.SetTrigger("action");
        //Quand on lance la compétence, le perso s'arrête, passe en reflect, et on lance l'animation.
        stopped = true;
        blocking = true;
        GameObject temp=Instantiate(burst, transform.position, Quaternion.identity);
        temp.GetComponent<MothBurst>().SetStats(gameObject, this);
    }



    public void EndComp()
    {
        stopped = false;
        blocking = false;
    }
}
