﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OmniSlash : MonoBehaviour
{

    public int avoidN;
    Character targetChar;
    LineRenderer lr;
    Vector2 targetV=Vector2.zero;
    bool started = false;
    public Transform[] startPos;

    public List<GameObject> touche;
    public int damage;
    public int force;
    public Gradient grad;


    private void Awake()
    {
        lr = GetComponent<LineRenderer>();
    }

    public void SetStats(int max, int avoid, Vector3 pos)
    {
        transform.position = pos;
        avoidN = avoid;
        int test = Random.Range(0, max);
        while (test == avoid)
        {
            test = Random.Range(0, max);
        }

        Character[] chars = FindObjectsOfType<Character>();

        foreach(Character cha in chars)
        {
            if (cha.playerNum == test)
            {
                targetChar = cha;
            }
        }

        StartCoroutine(Targeting());


    }

    private void Update()
    {
        lr.SetPosition(0, transform.position);
        lr.SetPosition(1, (Vector2)transform.position + targetV * 50);

        if (started)
        {
            


            for (int k = 0; k < startPos.Length; k++)
            {
                RaycastHit2D[] hits;
                hits = Physics2D.RaycastAll(startPos[k].transform.position, targetV, 70);

                for (int i = 0; i < hits.Length; i++)
                {
                    GameObject potato = hits[i].collider.gameObject;
                    if (potato.tag == "solid")
                    {

                    }
                    else
                    {


                        bool damaging = true;

                        if (touche.Count > 0)
                        {
                            for (int j = 0; j < touche.Count; j++)
                            {
                                if (touche[j] == potato)
                                {
                                    damaging = false;
                                }
                            }
                        }

                        if (damaging)
                        {
                            if (potato.tag == "player")
                            {
                                if (potato.GetComponent<Character>().playerNum != avoidN)
                                {
                                    potato.GetComponent<Character>().Damage(damage, avoidN);
                                    touche.Add(potato);
                                }
                                
                            }
                            else if (potato.tag == "smash")
                            {
                                Vector2 send = potato.transform.position - transform.position;
                                send.Normalize();
                                send *= force;
                                potato.GetComponent<SmashBall>().Damage(damage, avoidN, send);
                                touche.Add(potato);
                            }
                        }
                    }

                }
            }
        }
    }

    IEnumerator Targeting()
    {
        lr.startWidth = 0.2f;
        lr.endWidth = 0.2f;
        targetV = targetChar.transform.position - transform.position;
        targetV.Normalize();

        var mousePos = (Vector2)Camera.main.WorldToScreenPoint(transform.position) + targetV * 1000;

        var socketPos = Camera.main.WorldToScreenPoint(transform.position);
        mousePos.x = mousePos.x - socketPos.x;
        mousePos.y = mousePos.y - socketPos.y;
        var angle = Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg;

        //On adapte ensuite le tir pour qu'il soit dans l'angle visé
        transform.rotation = Quaternion.Euler(0, 0, angle);

        yield return new WaitForSeconds(1f);
        lr.startWidth = 1f;
        lr.endWidth = 1f;
        lr.colorGradient = grad;
        started = true;
        yield return new WaitForSeconds(0.55f);
        Destroy(gameObject);
    }
}
