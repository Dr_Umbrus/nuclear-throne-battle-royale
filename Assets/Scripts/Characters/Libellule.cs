﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Libellule : Character
{
    public int plusAngle;
    public float timer, maxTimer;
    bool started;
    public GameObject shadow;
    //public GameObject redTarget;



    public override void Pattern()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            Ultimate();
        }

        if (started)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                gun.multiPattern /= 2;
                anim.SetTrigger("endaction");
                gun.multishot /= 2;
                started = false;
                gun.maxAngle -= plusAngle;
            }
        }

    }

    public override void Competence()
    {
        anim.SetTrigger("action");
        gun.multiPattern *= 2;
        gun.maxAngle += plusAngle;
        gun.multishot *= 2;
        started = true;
        timer = maxTimer;
    }

    public override void Ultimate()
    {
        StartCoroutine(UltiLib());
    }

    IEnumerator UltiLib()
    {

        gm.Shake(5, 35, 1.05f);
        for (int i = 0; i < 10; i++)
        {
            Vector3 pos = Vector3.zero;
            pos.z = -8;
            pos.x = Random.Range(-10f, 10.1f);
            pos.y = Random.Range(-5f, 5.1f);
            pos.y += 3;
           GameObject temp= Instantiate(shadow, pos, Quaternion.identity);
            temp.GetComponent<RockFall>().ignore = playerNum;
            temp.GetComponent<RockFall>().timeFall +=i/10;

            if (i == 0)
            {
                temp.GetComponent<RockFall>().check = true;
            }
            yield return new WaitForSeconds(0.1f);
        }
    }
}
