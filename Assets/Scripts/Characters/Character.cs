﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;


public class Character : MonoBehaviour
{
    //La gestion des personnages. Note : Deviendra probablement un ancestor pour l'intégration des spells
    public GameObject paillettes;
    public int killStreak;

    public List<int> tresors;
    public int kills;

    bool stunned = false;
    public bool end;

    public bool blocking;

    public BoxCollider2D box;
    public bool stopped;

    public Slider lifebar;

    public float timerComp, maxTimerComp;

    //La direction de mouvement et de visée
    public Vector2 movement;
    public Vector2 aim;

    //Le game manager
    public GameManager gm;

    //Le sprite de mort (sert pour le respawn)
    public Sprite ded;
    //Un id pour savoir qui est qui
    public int playerNum;
    //La vitesse de mouvement
    public float speed;
    public float baseSpeed;
    //Le sprite renderer
    public SpriteRenderer sr;

    //L'arme du joueur
    public Gun gun;

    //L'endroit où on range l'arme
    public GameObject socket;
    //L'arme de départ 
    public GameObject startingGun;
    //Temps entre deux tirs
    public float cooldown=1;

    //Permet de savoir quand on peut ramasser des trucs
    public Floored sol;

    //Variables pour déterminer où on vise (voir plus tard)
    public Vector3 mousePos;
    public float angle;
    public Vector3 socketPos;

    //L'animator
    public Animator anim;

    //Les pv et pv max
    public int life;
    public int maxLife;

    //Le point de spawn, pour le début et le respawn
    public Vector3 spawnpoint;

    //Le temps où on peut pas se faire toucher
    public float timeinvincible;


    //Combien de temps est invincible au repop
    public float respawnTime = 2.5f;
    
    //Les 4 materials pour les outlines
    public Material[] outlines;

    public AudioSource cast;
    public AudioSource damaged;
    public AudioSource dedSon;
    public AudioSource blok;

    public GameObject can;
    public Text numberP;
    public Image fleche;
    Vector3 canPos;

    Rigidbody2D rb;

    public Color[] playerCol;

    public bool lockOn;

    bool shooting = false;


    public virtual void Starting()
    {

    }

    IEnumerator WaitSpawn()
    {
        stopped = true;
        yield return new WaitForSeconds(2);
        StartCoroutine(ShowLife());
        stopped = false;
    }

    //Appelé par le GM Permet de tout mettre en place
    public void SetStats(int number)
    {
        rb = GetComponent<Rigidbody2D>();
        canPos = can.transform.localPosition ;
        box = GetComponent<BoxCollider2D>();
        //On récupère les trucs importants
        anim = GetComponent<Animator>();
        
        gm = FindObjectOfType<GameManager>();
        if (sr == null)
        {
            sr = GetComponent<SpriteRenderer>();
        }
        Starting();

        //Le gm donne au joueur son identité, et on prend le material qui va avec
        playerNum = number;
        //sr.material = outlines[number];

        //On cherche les spawns et on trouve celui qui a la bonne id
        SpawnPoint[] spawns = FindObjectsOfType<SpawnPoint>();
        foreach (SpawnPoint spa in spawns)
        {
            if (spa.player == playerNum)
            {
                spawnpoint = spa.transform.position;
            }
        }
        spawnpoint = new Vector3(spawnpoint.x, spawnpoint.y, -5);
        //Puis on se tp dessus
        transform.position = spawnpoint;

        //On créé l'arme de départ et on la grab
        GameObject temp = Instantiate(startingGun, transform.position, Quaternion.identity);
        temp.GetComponent<Gun>().GrabMe(this);
        numberP.text = "P" + (playerNum + 1).ToString();
        numberP.color = playerCol[playerNum];
        fleche.color = playerCol[playerNum];
        Debug.Log(Gamepad.current);

        if (!gm.deathmatch)
        {
            for (int i = 0; i < 10; i++)
            {
                tresors.Add(1);
            }
        }

        lifebar.maxValue = maxLife;
        lifebar.value = maxLife;
        StartCoroutine(WaitSpawn());
        baseSpeed = speed;
    }



    //Récupère la valeur du stick gauche
    public void OnMove(InputValue value)
    {
        Vector2 potato = value.Get<Vector2>();
        if (potato.x < 0.4 && potato.x > -0.4)
        {
            potato.x = 0;
        }
        if (potato.y < 0.4 && potato.y > -0.4)
        {
            potato.y = 0;
        }
        movement = potato;

        

    }
    //Récupère la valeur du stick droit
    public void OnLook(InputValue value)
    {
        if (!lockOn)
        {
            Vector2 potato = value.Get<Vector2>();
            if (potato.x > 0.1 || potato.x < -0.1 || potato.y > 0.1 || potato.y < -0.1)
            {
                aim = value.Get<Vector2>();
            }
        }
        
        
    }

    //S'active quand on appuie sur le bouton pour ramasser
    public void OnGrab()
    {
        //On ramasse une arme, s'il y en a une
        if (sol != null)
        {
            sol.GrabGun(this);
        }
        
    }


    public void OnSpell()
    {
        if (timerComp <= 0 && !stunned)
        {
            if (killStreak < 4)
            {
                cast.Play();
                timerComp = maxTimerComp;
                Competence();
            }
            else
            {
                Ultimate();
            }
        }
    }
    //Quand on appuie sur le bouton pour tirer
    public void OnFire()
    {

        shooting = true;
        
        
    }

    public void OnFire1()
    {

        shooting = false;

    }

    //La visée
    public void Aiming()
    {
        //On casse le vecteur de visée en variables
        float aH = aim.x;
        float aV = aim.y;

        //On prend un point exagérément loin
        mousePos = Camera.main.WorldToScreenPoint(socket.transform.position) + new Vector3(aH, aV, 0) * 1000;
        socketPos = Camera.main.WorldToScreenPoint(socket.transform.position);
        //On créé de nouvelles infos avec le point visé par rapport au point d'ancrage de l'arme
        mousePos.x = mousePos.x - socketPos.x;
        mousePos.y = mousePos.y - socketPos.y;
        //Puis on en tire l'angle, pour pouvoir orienter la socket dans la bonne direction, et donc viser où on veut
        angle = Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg;
        socket.transform.rotation = Quaternion.Euler(0, 0, angle);
    }

    //Le déplacement
    public void Movement()
    {
        
        //On casse le vecteur de mouvement
        float h = movement.x;
        float v = movement.y;

        //Si le joueur va à gauche on le flip, sinon on le déflip
        if (h < -0.4)
        {
            if (sr.flipX == false)
            {
                sr.flipX = true;
            }
        }
        else if (h > 0.4)
        {
            if (sr.flipX == true)
            {
                sr.flipX = false;
            }
        }

        //Déplacements, avec un /10 parce que le nouveau système faisait des sanics
        transform.Translate(new Vector2(h, v) * speed/10 * Time.deltaTime);

        //On récupère une variable avec le mouvement, pour définir si le perso bouge, et lancer l'anim de marche
        float move = (Mathf.Abs(h) + Mathf.Abs(v)) * speed;
        anim.SetFloat("movement", move);
    }

    public virtual void Pattern()
    {

    }


    public void FixedUpdate()
    {

        /*if (rb.velocity.x > 0.5 || rb.velocity.y > 0.5 || rb.velocity.y < -0.5 || rb.velocity.x < -0.5)
        {
            stunned = true;
        }
        else
        {
            stunned = false;
        }*/
        if (shooting)
        {
            if (!end && !stopped && !stunned)
            {
                //On vérifie de bien avoir une arme (même si normalement c'est impossible de pas en avoir)
                if (gun != null)
                {
                    //On vérifie bien que le cd est fini, qu'on a rechargé, et qu'on est pas en train de tirer
                    if (cooldown <= 0 && !gun.shooting && !gun.attacking)
                    {
                        //Puis on lance le gunning (voir Gun) et le cooldown)
                        gun.Gunning();
                        cooldown = gun.cooldown;
                    }
                }
            }
        }
        can.transform.localPosition = canPos;
        if (!end)
        {
            if (!stopped && !stunned)
            {
                //Movement et Aiming se lancent à chaque frame
                Movement();
               
            }

            Aiming();

            //Si on a une arme en main, et qu'on est pas en train de tirer/recharger, on descend le cooldown
            if (gun != null)
            {
                if (cooldown <= 0 && !gun.shooting && !gun.attacking)
                {
                }
                else
                {
                    if (cooldown > 0)
                    {
                        cooldown -= Time.deltaTime;
                    }

                }

            }

            if (timerComp > 0)
            {
                timerComp -= Time.deltaTime;
            }

            Pattern();
        }
        
    }

    //Quand on respawn
    public void Respawn()
    {
        killStreak = 0;
        //On revient full vie, au point de spawn, avec X temps invincible
            life = maxLife;
            transform.position = spawnpoint;
        StartCoroutine(ShowLife());
        GameObject temp = Instantiate(startingGun, transform.position, Quaternion.identity);
        temp.GetComponent<Gun>().GrabMe(this);
    }

    //Quand on entre dans le trigger d'une arme au sol, on peut la prendre
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "floorG")
        {

            sol = collision.gameObject.GetComponent<Floored>();
        }

    }

    //Quand on sort du trigger d'une arme au sol, on peut plus la prendre
    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "floorG")
        {
            sol = null;
        }
    }

    

    //Quand on prend des dégâts
    public void Damage(int dam, int source)
    {
        if (!blocking)
        {

            gm.Shake(0.5f, 10, 1.05f);

            //On peut seulement blesser un joueur encore en vie
            if (life > 0)
            {

                damaged.Play();
                Destroy(Instantiate(paillettes, transform.position, Quaternion.identity), 1);
                //Il perd de la vie en fonction des damages
                life -= dam;

                //S'il meurt, on active l'animation, la coroutine de repop, et on dit au gm qui c'est qui a fait le kill
                if (life <= 0)
                {
                    dedSon.Play();
                    life = 0;
                    StartCoroutine(RespawnTime());
                    gm.Kill(source);
                    gun.Discard();

                    if (tresors.Count >= 1)
                    {
                        int potato = Mathf.CeilToInt(tresors.Count / 2);
                        for (int i = 0; i < potato; i++)
                        {
                            if (tresors[0] == 1)
                            {
                                gm.SummonTreasure(transform.position);
                            }
                            else
                            {
                                gm.SummonBigTreasure(transform.position, source);
                            }

                            tresors.RemoveAt(0);
                        }
                    }
                }
                //Sinon on lance l'anim de damages
                else
                {
                    anim.SetTrigger("damage");
                    timeinvincible = 3;
                }
            }
        }
        else
        {
            blok.Play();
        }
        StopCoroutine(ShowLife());
        StartCoroutine(ShowLife());
    }
    

    public virtual void Competence()
    {

    }


    //Pour respawn
    public IEnumerator RespawnTime()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, 0);
        box.isTrigger = true;
        stopped = true;
        anim.SetTrigger("dead");

        

        //On reste mort ce temps
        yield return new WaitForSeconds(2);
        //Puis on respawn
        Respawn();
        //Trigger pour passer du sprite ded au idle
        anim.SetTrigger("alive");

        stopped = false;
        box.isTrigger = false;

        blocking = true;
        yield return new WaitForSeconds(1.5f);
        blocking = false;
    }


    public void AddLoot(int value)
    {
        tresors.Add(value);        
    }

    public int AskScore()
    {
        int potato = 0;

        if (gm.deathmatch)
        {
            potato = kills;
        }

        else
        {
            if (tresors.Count > 0)
            {
                for (int i = 0; i < tresors.Count; i++)
                {
                    potato += tresors[i];
                }
            }

            
        }

        return potato;

    }

    IEnumerator ShowLife()
    {
        lifebar.gameObject.SetActive(true);
        lifebar.value = life;
        yield return new WaitForSeconds(1.5f);
        if (life > 5)
        {
            lifebar.gameObject.SetActive(false);
        }

    }

    public virtual void Ultimate()
    {

    }

}
