﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MothBurst : MonoBehaviour
{
    public int damage;
    public GameObject creatorOb;
    public Moth creator;
    CircleCollider2D col;

    private void Awake()
    {
        col = GetComponent<CircleCollider2D>();
        col.enabled = false;
    }

    public void SetStats(GameObject obj, Moth moth)
    {
        creator = moth;
        creatorOb = obj;
        Destroy(gameObject, 0.75f);
        col.enabled = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "boss")
        {
            collision.GetComponent<Boss>().Damaged(damage, creator.playerNum);
        }
        else if(collision.gameObject.tag=="player" && collision.gameObject != creatorOb)
        {
            collision.GetComponent<Character>().Damage(damage, creator.playerNum);
        }
    }

    private void OnDestroy()
    {
        creator.EndComp();
    }
}
