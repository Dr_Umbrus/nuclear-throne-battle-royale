﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cococinelle : Character
{
    public GameObject omegaTide;
    public AudioSource endSpell;
    //Si on est en train de bloquer
    bool block = false;

    //Timer de block et durée max du block
    public float timeBlock, maxTimeBlock;




    public override void Pattern()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            Ultimate();
        }
        //Si on est en train de bloquer
        if (block)
        {
            //On vérifie le timer
            if (timeBlock > 0)
            {
                timeBlock -= Time.deltaTime;
                //Le cd du spell ne se fait pas tant qu'on est actif
                timerComp = maxTimerComp;
            }
            else
            {
                endSpell.Play();
                anim.SetTrigger("endshield");
                //Voir OnEnd.
                block = false;
                //anim.SetTrigger("release");
                speed = baseSpeed;
                blocking = false;
            }
        }
    }


    //Quand on lance le spell
    public override void Competence()
    {
        anim.SetTrigger("shield");
        //On  met le temps de block au max
        timeBlock = maxTimeBlock;
        //On bloque
        block = true;
        //Le perso est ralentit
        speed = baseSpeed / 2;
        //Et on active le blocage de dégâts
        blocking = true;
    }

    public override void Ultimate()
    {
        StartCoroutine(Omega());
    }

    IEnumerator Omega()
    {
        stopped = true;
        box.enabled = false;


        Instantiate(omegaTide, transform.position, Quaternion.identity);
        yield return new WaitForSeconds(1f);
        for (int i = 0; i < 5; i++)
        {
            GameObject temp = Instantiate(omegaTide, transform.position, Quaternion.identity);

            temp.transform.rotation = Quaternion.Euler(0, 0, angle + 90 + 72 * i);
            temp.transform.Translate((Vector3.up * 4), Space.Self);
            temp.transform.rotation = Quaternion.identity;
        }
        yield return new WaitForSeconds(1f);
        for (int i = 0; i < 5; i++)
        {
            GameObject temp = Instantiate(omegaTide, transform.position, Quaternion.identity);

            temp.transform.rotation = Quaternion.Euler(0, 0, angle + 45 + 72 * i);
            temp.transform.Translate((Vector3.up * 8), Space.Self);
            temp.transform.rotation = Quaternion.identity;
        }
        yield return new WaitForSeconds(1f);
        for (int i = 0; i < 10; i++)
        {
            GameObject temp = Instantiate(omegaTide, transform.position, Quaternion.identity);

            temp.transform.rotation = Quaternion.Euler(0, 0, angle + 90 + 36 * i);
            temp.transform.Translate((Vector3.up *12), Space.Self);
            temp.transform.rotation = Quaternion.identity;
        }
        stopped = false;
        box.enabled = true;
    }
}
