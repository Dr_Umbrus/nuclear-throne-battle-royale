﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaguetteShot : Projectile
{
    public GameObject explosion;


    public override void Pattern()
    {
        //Il va en ligne droite
        transform.Translate((Vector3.up * speed * Time.deltaTime), Space.Self);
    }

    public override void Touch(Collider2D collision)
    {
        //Il meurt contre les murs
        Destroy(gameObject);

    }

    public override void HitPlayer(Collider2D collision)
    {
        //Et il meurt en faisant des dégâts au joueur
        collision.GetComponent<Character>().Damage(damage, player);
        Destroy(gameObject);
    }


    private void OnDestroy()
    {
        GameObject temp =Instantiate(explosion, transform.position, transform.rotation);
        temp.GetComponent<Projectile>().Create(player);

    }
}
