﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaguetteExplosion : Projectile
{

    SpriteRenderer sr;
    float timer=0;
    CircleCollider2D col;

    public override void Starting()
    {
        sr = GetComponent<SpriteRenderer>();
        col = GetComponent<CircleCollider2D>();
        col.enabled = false;
        StartCoroutine(WaitCol());
    }

    public override void Pattern()
    {
        sr.color = Color.Lerp(Color.white, Color.clear, timer);
        timer += Time.deltaTime*1.25f;
        if (sr.color.a < 0.25)
        {
            col.enabled = false;
        }
        if (timer >= 1)
        {
            Destroy(gameObject);
        }
    }


    public override void HitPlayer(Collider2D collision)
    {
        //Et il meurt en faisant des dégâts au joueur
        collision.GetComponent<Character>().Damage(damage, player);
    }

    IEnumerator WaitCol()
    {
        yield return 0;
        col.enabled = true;
    }
}
