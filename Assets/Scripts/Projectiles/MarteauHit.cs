﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarteauHit : Projectile
{
    CircleCollider2D cc;
    public float maxSize;
    float timer = 0;

    public override void Starting()
    {
        cc = GetComponent<CircleCollider2D>();
    }
    public override void Pattern()
    {
        timer += Time.deltaTime * speed;
        cc.radius = Mathf.Lerp(0, maxSize, timer);

        if (timer >= 1)
        {
            cc.enabled = false;
            StartCoroutine(Dying());
        }
    }

    public override void HitPlayer(Collider2D collision)
    {
        collision.GetComponent<Character>().Damage(damage, player);
    }
}
