﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    //Classe générale pour les projectiles

    public float force = 0;
        //Temps avant destruction (mettre 1000 si le tir s'arrête seulement sur un mur
    public float lifetime;
    //Vitesse de déplacement
    public float speed;
    //Nombre de dégâts sur le joueur
    public int damage;
    //Qui l'a créé
    public int player;
    public Character character;
    public AudioSource noise;

    public bool ignore = false;
    public bool hitAndRun = false;

    public bool staticShot=false;
    public bool souffle = false;

    //Au spawn, on récupère l'id du joueur qui l'a créé, et on met un timer pour mourir.
    public void Create(int j)
    {
        player = j;
        Destroy(gameObject, lifetime);

        var persos = FindObjectsOfType<Character>();
        for (int i = 0; i < persos.Length; i++)
        {
            if (persos[i].playerNum == player)
            {
                character = persos[i];
            }
        }
        if (!souffle)
        {
            Vector2 knockBack = character.transform.position - transform.position;
            knockBack.Normalize();
            character.GetComponent<Rigidbody2D>().AddForce(knockBack * force * 350);
        }
        
    }

    //Lance la fonction Starting au lancement
    public void Awake()
    {
        noise = GetComponent<AudioSource>();
        Starting();
    }

    //Foncton virtuelle pour tout ce qui se passe au lancement
    public virtual void Starting()
    {

    }

    //Lancer le Pattern
    public void FixedUpdate()
    {
        Pattern();
    }

    //Fonction virtuelle pour l'action à chauqe frame.
    public virtual void Pattern()
    {

    }


    public void OnTriggerEnter2D(Collider2D collision)
    {
        Collided(collision);

        //Si le projectile entre en contact avec un joueur qui n'est pas son créateur, on lance HitPlayer
        if (collision.gameObject.tag == "player")
        {
            Character temp = collision.gameObject.GetComponent<Character>();
            if (temp.playerNum != player)
            {
                
                if (staticShot)
                {
                    Vector2 knockBack = temp.transform.position - transform.position;
                    knockBack.Normalize();
                    temp.GetComponent<Rigidbody2D>().AddForce(knockBack * force * 750);
                }
                else
                {
                    Vector2 knockBack = Quaternion.AngleAxis(transform.rotation.z, Vector3.forward) * Vector3.right;
                    knockBack.Normalize();
                    temp.GetComponent<Rigidbody2D>().AddForce(knockBack * force * 750);
                }
                

                {
                    HitPlayer(collision);
                }

                
            }
            
        }
        else if (collision.gameObject.tag == "smash")
        {
            Vector2 send=collision.transform.position-transform.position;
            send.Normalize();
            send*=force;
            collision.GetComponent<SmashBall>().Damage(damage, player, send);
        }
        else
        {
            if (collision.gameObject.tag == "solid")
            {
                Touch(collision);
            }
        }

    }

    public virtual void Collided(Collider2D collision)
    {

    }

    
   
    //Fonction virtuelle en touchant un mur
    public virtual void Touch(Collider2D collision)
    {

    }

    //Fonction virtuelle en touchant un joueur
    public virtual void HitPlayer(Collider2D collision)
    {

    }

    public IEnumerator Dying()
    {
        if (GetComponent<Collider2D>() != null)
        {
            GetComponent<Collider2D>().enabled = false;
        }
       
        if(GetComponent<SpriteRenderer>() != null)
        {
            GetComponent<SpriteRenderer>().color = Color.clear;
        }
        else
        {
            GetComponentInChildren<SpriteRenderer>().color = Color.clear;
        }

        if (GetComponent<TrailRenderer>() != null)
        {
            GetComponent<TrailRenderer>().enabled=false;
        }

        if (noise != null)
        {
            while (noise.isPlaying)
            {
                yield return 0;
            }
        }
        

        Destroy(gameObject);
    }
}
