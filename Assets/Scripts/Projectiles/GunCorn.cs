﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunCorn : Projectile
{
    //La taille du collider une fois grossis
    public float maxRad;
    //Le sprite où la transformation commence
    public Sprite transformation;
    //Le collider
    CircleCollider2D cc;
    //Est-ce qu'on est déjà gros
    bool big = false;
    SpriteRenderer sr;
    AudioSource audSo;


    public override void Starting()
    {
        //récupérer les components
        audSo = GetComponent<AudioSource>();
        sr = GetComponent<SpriteRenderer>();
        cc = GetComponent<CircleCollider2D>();
    }

    public override void Pattern()
    {
        //On bouge tout droit
        transform.Translate((Vector3.up * speed * Time.deltaTime), Space.Self);

        //Quand le sprite arrive, si on s'est pas encore transformé
        if(sr.sprite==transformation && !big)
        {
            //On passe en grosse collision, on valide la transfo, les dégâts augmentent, et on joue le pop
            cc.radius = maxRad;
            big = true;
            damage *= 2;
            audSo.Play();
        }
    }


    //Voir tir simple
    public override void Touch(Collider2D collision)
    {
        Destroy(gameObject);
        
    }

    public override void HitPlayer(Collider2D collision)
    {
        collision.GetComponent<Character>().Damage(damage, player);
        Destroy(gameObject);
    }

}
