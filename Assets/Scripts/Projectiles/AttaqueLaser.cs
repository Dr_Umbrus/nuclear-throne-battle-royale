﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttaqueLaser : Projectile
{
    GameManager gm;
    public Gradient grad;
    public LayerMask Mask;
    public LayerMask Mask2;
    public Character creator;
    public Transform muzzle;
    public Transform hitPos;
    public LineRenderer laser;
    float timer = 0;
    public float duration;
    bool lockedOn = false;
    public Vector2 mousePos, socketPos;
    float angle=0;
    bool ending=false;
    public Transform[] checkLaser;

    public AudioClip laz2;

    bool started = false;

    List<GameObject> touche=new List<GameObject>();
    

    public override void Starting()
    {
        StartCoroutine(Waiting());
        gm = FindObjectOfType<GameManager>();
    }

    IEnumerator Waiting()
    {
        yield return 0;
        creator = character;
        creator.stopped = true;
        muzzle = creator.gun.muzzle;
        started = true;
    }

    public override void Pattern()
    {
        if (started)
        {


            if (creator.life <= 0)
        {
                ending = true;
                timer = 0.75f;
                creator.lockOn = false;
                creator.gun.lockOn = false;
                Destroy(gameObject);
        }
            if (muzzle != null)
            {
                Vector2 targeting = creator.aim;

                //On prend un point exagérément loin dans ce vecteur, pour avoir une meilleure précision
                mousePos = Camera.main.WorldToScreenPoint(muzzle.position) + new Vector3(targeting.x, targeting.y, 0) * 1000;

                //On considère l'endroit où se trouve actuellement la socket
                socketPos = Camera.main.WorldToScreenPoint(muzzle.transform.position);
                //On casse ensuite le vecteur, en prenant en compte la différence de position entre le point visé et la socket
                mousePos.x = mousePos.x - socketPos.x;
                mousePos.y = mousePos.y - socketPos.y;
                //Puis on calcule l'angle en radian qui en résulte, avant de le repasser en degrés pour avoir une valeur utile entre 180 et -180;
                angle = Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg;

                //Avec tout ça, on peut donc simuler l'angle et la direction du tir

                //Ensuite, on vérifie si le joueur est bien en train de viser, en regardant qu'on est au dessus de 0.25 dans au moins une direction. Le test est en valeur absolue pour prendre en compte la gauche et le bas
                if (Mathf.Abs(mousePos.x / 1000) > 0.25 || Mathf.Abs(mousePos.y / 1000) > 0.25)
                {
                    mousePos.Normalize();
                    hitPos.position = new Vector2(muzzle.position.x, muzzle.position.y) + mousePos * 40;



                }
                Vector3 hitVec = hitPos.position - muzzle.position;
                laser.SetPosition(0, muzzle.position);
                laser.SetPosition(1, muzzle.position + hitVec / 7);
                laser.SetPosition(2, muzzle.position + hitVec / 7 * 2);
                laser.SetPosition(3, muzzle.position + hitVec / 7 * 3);
                laser.SetPosition(4, muzzle.position + hitVec / 7 * 4);
                laser.SetPosition(5, muzzle.position + hitVec / 7 * 5);
                laser.SetPosition(6, muzzle.position + hitVec / 7 * 6);
                laser.SetPosition(7, hitPos.position);
            }



            
            
        
        if (!lockedOn)
        {
            

            timer += Time.deltaTime;

            /*laser.startColor = Color.Lerp(Color.red, Color.white, timer);
            laser.endColor = Color.Lerp(Color.red, Color.white, timer);*/
            laser.startWidth = Mathf.Lerp(0, 1, 1-timer);
            laser.endWidth = 0;

            if (timer >= 1)
            {
                    gm.Shake(5, 60, 1.05f);
                    laser.startWidth = 3;
                laser.endWidth = 3;
                lockedOn = true;
                //laser.colorGradient = grad;
                creator.lockOn = true;
                creator.gun.lockOn = true;
                    GetComponent<AudioSource>().clip = laz2;
                    GetComponent<AudioSource>().Play();
            }

        }
        else if(!ending)
        {
            mousePos = hitPos.position - muzzle.position;

            
            for (int k = 0; k < checkLaser.Length; k++)
            {
                RaycastHit2D[] hits;
                hits = Physics2D.RaycastAll(checkLaser[k].transform.position, mousePos, 50);

                for (int i = 0; i < hits.Length; i++)
                {
                    GameObject potato = hits[i].collider.gameObject;
                    if (potato.tag == "solid")
                    {
                        //hitPos.position = hits[i].point;

                    }
                    else
                    {
                        

                        bool damaging = true;

                        if (touche.Count > 0)
                        {
                            for (int j = 0; j < touche.Count; j++)
                            {
                                if (touche[j] == potato)
                                {
                                    damaging = false;
                                }
                            }
                        }

                        if (damaging)
                        {
                            if (potato.tag == "player" && potato!=creator.gameObject)
                            {
                                potato.GetComponent<Character>().Damage(damage, player);
                                touche.Add(potato);
                            }
                            else if (potato.tag == "smash")
                                {
                                    Vector2 send = potato.transform.position - transform.position;
                                    send.Normalize();
                                    send *= force;
                                    potato.GetComponent<SmashBall>().Damage(damage, player, send);
                                }
                            }
                    }

                }
            }
            

                   
            duration -= Time.deltaTime/2;
            if (duration <= 0)
            {
                ending = true;
                timer = 1;
                creator.lockOn = false;
                creator.gun.lockOn = false;
                creator.stopped = false;
            }
        }
        else
        {

            
            timer -= Time.deltaTime*5;

            /*laser.startColor = Color.Lerp(Color.red, Color.white, timer);
            laser.endColor = Color.Lerp(Color.red, Color.white, timer);*/
            laser.startWidth = Mathf.Lerp(0, 3, timer);
            laser.endWidth = Mathf.Lerp(0, 3, timer);

            if (timer <= 0)
            {
                Destroy(gameObject);
            }
        }


            if (muzzle != null)
            {
                transform.rotation = creator.gun.transform.rotation;
            }
        

        }
    }
}
