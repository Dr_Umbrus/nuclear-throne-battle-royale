﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomboursin : Projectile
{
    //Est-ce qu'il est accroché
    bool hugging = false;
    //Les piti pics
    public GameObject minirsin;
    //Au bout de combien de temps il s'arrête sur le sol s'il est pas accroché
    public float timeToStop;
    //Sur quoi il est accroché
    public Transform target=null;
    //Avec quel offset
    public Vector3 offset;

    public override void Pattern()
    {
        //S'il est pas accroché, il va tout droit
        if (!hugging)
        {
            transform.Translate((Vector3.up * speed * Time.deltaTime), Space.Self);
        }
        //Sinon il suit ce qui le tient. Note : J'ai pas utilisé un système de parent parce que ça faisait des déformations moches sur les murs
        else
        {
            transform.position = target.position + offset;
        }

        //Timer
        if (timeToStop > 0)
        {
            timeToStop -= Time.deltaTime;
        }
        //S'il est pas accroché à la fin du timer, il s'accroche à sa position
        else if (!hugging)
        {
            hugging = true;
            target = transform;
            offset = Vector3.zero;
        }
    }


    //Quand il touche un truc, il prend sa position par rapport à l'objet, s'accroche dessus, et le note comme cible
    public override void Touch(Collider2D collision)
    {
        offset = transform.position - collision.transform.position;
        hugging = true;
        target = collision.gameObject.transform;
    }

    public override void HitPlayer(Collider2D collision)
    {
        offset = transform.position - collision.transform.position;
        hugging = true;
        target = collision.gameObject.transform;
    }

    private void OnDestroy()

    {
        //Au moment de se détruire, on fait 8 petits pics, sur les angles 0/45/90/135/180/-45/-90/-135
        for(int i=0; i<8; i++)
        {
            GameObject temp = Instantiate(minirsin, transform.position, Quaternion.identity);

            temp.transform.Rotate(new Vector3(0, 0, 90+45*i));
            temp.GetComponent<Projectile>().Create(player);
        }
    }
}
