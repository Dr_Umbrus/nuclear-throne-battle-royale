﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crossbone : Projectile
{
    Animator anim;
    SpriteRenderer sr;
    public Sprite dead;
    bool dying = false;

    public override void Starting()
    {
        anim = GetComponentInChildren<Animator>();
        sr = GetComponentInChildren<SpriteRenderer>();
    }


    public override void Pattern()
    {
        //Si on est pas en train de mourir, on va tout droit
        if (!dying)
        {
            transform.Translate((Vector3.up * speed * Time.deltaTime), Space.Self);
        }
        else
        {
            //On se détruit au dernier sprite de l'animation
            if (sr.sprite == dead)
            {
                Destroy(gameObject);
            }
        }

    }

    
    public override void HitPlayer(Collider2D collision)
    {
        if (!dying)
        {
            //On fait les dégâts, mais sans se détruire ==> Traverse les joueurs
            collision.GetComponent<Character>().Damage(damage, player);
        }
        
    }


    public override void Touch(Collider2D collision)
    {
        //Mettre en place la séquence de mort quand on touche un mur
        dying = true;
        anim.SetTrigger("dead");
    }
}
