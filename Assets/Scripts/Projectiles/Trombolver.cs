﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trombolver : Projectile
{
    //l'amplitude de la sine (à quel point ça fait des grands mouvements)
    public float amp;
    //La fréquence de la sine (le temps entre deux retour au point de départ
    public float freq;
    //Les notes
    public Sprite[] notes;
    SpriteRenderer sr;
    //Les 7 couleurs
    public Color[] rainbow;
    //La vitesse de changement de couleur
    public float changeTime, maxChangeTime;
    //Noter à quelle couleur on en est pour savoir quand loop
    public int myColor;

    public override void Starting()
    {
        //On prend le sr en se créant, puis on devient une note random
        sr = GetComponentInChildren<SpriteRenderer>();
        sr.sprite = notes[Random.Range(0, notes.Length)];
    }


    public override void Pattern()
    {
        //On avance tout droit
        transform.Translate((Vector3.up * speed * Time.deltaTime), Space.Self);

        //Puis on rajoute la sinusoïde par dessus
        transform.position = transform.position + transform.right * Mathf.Sin(Time.time * freq) * amp;
        //Et on augmente l'amplitude
        amp += Time.deltaTime/4;


        //Timer pour changer de couleur
        if (changeTime > 0)
        {
            changeTime -= Time.deltaTime;

        }
        else
        {
            changeTime = maxChangeTime;
            //Bien vérifier qu'on reste dans les limites de l'Array
            if (myColor == rainbow.Length - 1)
            {
                myColor = 0;
            }
            else
            {
                myColor++;
            }
            //et on change de couleur
            sr.color = rainbow[myColor];
        }
    }


    //Voir tir simple
    public override void Touch(Collider2D collision)
    {
        Destroy(gameObject);

    }

    public override void HitPlayer(Collider2D collision)
    {
        collision.GetComponent<Character>().Damage(damage, player);
        Destroy(gameObject);
    }
}
