﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomerangShot : Projectile
{
    GameObject creator;
    public Vector3 target, spawn;
    float timer = 0;
    bool retour=false;
    GameObject gun;
    public bool started = false;
    public float dist;


    public override void Starting()
    {
        hitAndRun = true;
        StartCoroutine(TargetAcquired());
    }

    IEnumerator TargetAcquired()
    {
        yield return 0;
        
        
        Character[] potato = FindObjectsOfType<Character>();
        foreach (Character dude in potato)
        {
            if (dude.playerNum == player)
            {
                creator = dude.gameObject;
            }
        }

        spawn = creator.transform.position;

        Vector3 aim = new Vector3(transform.position.x - spawn.x, transform.position.y - spawn.y, 0);
        aim.Normalize();
        aim *= dist;
        target = (transform.position + aim);

        gun = creator.GetComponent<Character>().gun.gameObject;
        started = true;
    }

    public override void Pattern()
    {
        if (started)
        {
            transform.Rotate(new Vector3(0, 0, 20));
            transform.position = Vector3.Lerp(spawn, target, timer);
            timer += Time.deltaTime*speed;
            if (retour)
            {
                target = creator.transform.position;
            }

            if (timer >= 1)
            {
                Retour();
            }
        }
    }

    public void Retour()
    {
        spawn = transform.position;
        retour = true;
        timer = 0;
    }

    public override void Touch(Collider2D collision)
    {
        Retour();

    }

    public override void HitPlayer(Collider2D collision)
    {
        collision.GetComponent<Character>().Damage(damage, player);
    }


    public override void Collided(Collider2D collision)
    {
        //Si le projectile entre en contact avec un joueur qui n'est pas son créateur, on lance HitPlayer
        if (collision.gameObject.tag == "player")
        {
            Character temp = collision.gameObject.GetComponent<Character>();
            if (retour)
            {
                
                if (temp.playerNum == player)
                {
                    temp.gun.Visual();

                    Destroy(gameObject);

                }
            }
        }
    }


    private void OnTriggerStay2D(Collider2D collision)
    {
        Character temp = collision.gameObject.GetComponent<Character>();
        if (temp.playerNum == player)
        {
            if (creator.GetComponent<Character>().gun.tag == "Boom")
            {
                temp.gun.GetComponent<Boomerang>().Visual();
            }

            Destroy(gameObject);

        }
    }

}
