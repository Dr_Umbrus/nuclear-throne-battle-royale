﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmashBall : MonoBehaviour
{
    public int life;
    bool bouncy = false;
    public Rigidbody2D rb;
    public int speed;
    Vector2 direction;
    public GameObject treasure, bigTreasure, endPart;
    int damageReceived = 0;
    public int threshold;
    public bool dying;
    public StatsManager sm;
    ParticleSystem ps;
    AudioSource aud;

    private void Awake()
    {
        aud = GetComponent<AudioSource>();
        ps = GetComponent<ParticleSystem>();
        rb = GetComponent<Rigidbody2D>();
        direction = Random.insideUnitCircle;
        direction.Normalize();
        Destroy(gameObject, 12);
    }

    private void Update()
    {
        if (!bouncy)
        {
            transform.Translate(direction * speed * Time.deltaTime);
        }

        if (bouncy)
        {

            if (rb.velocity.magnitude < 0.2f)
            {
                
                rb.velocity = Vector2.zero;
                bouncy = false;
                direction = Random.insideUnitCircle;
                direction.Normalize();
            }
        }

        
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!bouncy)
        {
            direction = Random.insideUnitCircle;
            direction.Normalize();
        }
       /* else
        {
            rb.velocity = -rb.velocity;
        }*/
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Teleporter>() != null)
        {
            direction = -direction;
        }
    }

    public void Damage(int dam, int source, Vector2 force)
    {
        if (!dying)
        {
            life -= dam;
            damageReceived += dam;
            if (damageReceived > threshold)
            {
                damageReceived = 0;
                Vector2 pos = new Vector2(transform.position.x, transform.position.y);
                for(int i=0; i<10; i++)
                {
                    Instantiate(treasure, pos+Random.insideUnitCircle * 2, Quaternion.identity);
                }
                
            }

            bouncy = true;
            Vector2 targetSens = force;
            targetSens.Normalize();
            rb.velocity = targetSens * rb.velocity.magnitude;
            rb.velocity += force;

            if (life <= 0)
            {
                for (int i = 0; i < 4; i++)
                {
                    Vector2 pos = new Vector2(transform.position.x, transform.position.y);
                    GameObject temp = Instantiate(bigTreasure, pos+Random.insideUnitCircle*2, Quaternion.identity);
                    temp.GetComponent<BigTreasure>().SetTarget(source);
                    
                    
                }

                
                Instantiate(endPart, transform.position, Quaternion.identity);
                Destroy(gameObject);
            }
            else
            {
                aud.Play();
            }
        }

    }

    private void OnDestroy()
    {
        sm.EndSmash();
    }
}

