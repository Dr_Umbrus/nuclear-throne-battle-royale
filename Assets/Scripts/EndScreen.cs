﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScreen : MonoBehaviour
{
    //L'écran de résultats


    GameManager gm;

    //Les scores en texte
    public Text[] scores;
    //Les 4 personnages à placer
    public Image[] characters;
    //Les trois récompenses + la pierre tombale
    public GameObject[] trophees;

    //Les sprites vivant et morts
    public Sprite[] living;
    public Sprite[] dead;
    //Les 4 couleurs de fonts
    public Color[] fonts;
    //Le classement, sous forme d'ints
    public int[] classement= new int[4];

    public int[] scoresInt = new int[4];

    private void Awake()
    {
        gm=FindObjectOfType<GameManager>();


        for (int i = 0; i < gm.nPlayer; i++)
        {
            scoresInt[i] = gm.scores[i];
        }
        //On lance l'organisation des scores
        OrderResults();

        //Ensuite on met en place le visuel
        for(int i=0;i<gm.nPlayer; i++)
        {
            //On active autant de personnages et récompenses qu'il y a de joueurs
            characters[i].gameObject.SetActive(true);
            trophees[i].SetActive(true);
            scores[i].gameObject.SetActive(true);

            //Les joueurs avant le quatrième sont vivants, et le dernier est mort
            if (i != 3)
            {
                characters[i].sprite = living[gm.chosenCharacters[classement[i]]];
            }
            else
            {
                characters[i].sprite = dead[gm.chosenCharacters[classement[i]]];
            }

            //On met les scores dans la couleur du joueur associé et on update le texte
            scores[i].color = fonts[classement[i]];
            scores[i].text = scoresInt[classement[i]].ToString() + " POINTS";
        }

        
    }

    private void Update()
    {
        //Retour au menu
        if(Input.anyKeyDown)
        {
            gm.BackMenu();
        }
    }

    //Mettre les scores dans l'ordre pour classer les joueurs
    //Note : Ne gère pas les égalités actuellement. PAs d'idée sur comment régler ça
    public void OrderResults()
    {
        //Un tableau où ranger ça pour l'instant
            int[] scores = new int[4];

            for (int i = 0; i < 4; i++)
            {
                scores[i] = -1;
                classement[i] = -1;
            }


            //Pour le nombre de joueurs => taille du classement
            for (int i = 0; i < gm.nPlayer; i++)
            {
                //Vérifier chaque joueur
                for (int j = 0; j < gm.nPlayer; j++)
                {
                //Si le score du joueur est plus haut que le score actuel
                    if (scoresInt[j] > scores[i])
                    {
                    bool test = true;
                    //On vérifie le classement
                    for (int k = 0; k < classement.Length; k++)
                    {
                        //Si aucun point du classement n'est déjà pris par ce joueur
                        if (classement[k] == j)
                        {
                            test = false;
                        }
                    }
                    //On ajoute le score du joueur.
                    if (test)
                    {
                        scores[i] = scoresInt[j];
                        classement[i] = j;
                    }
                    
                    }
                }
                
            }

            


    }
}
