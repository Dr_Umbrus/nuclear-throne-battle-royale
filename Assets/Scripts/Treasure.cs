﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Treasure : MonoBehaviour
{
    public int value;
    bool ramasse=false;
    public AudioSource sounding;


    private void Awake()
    {
        transform.Translate(new Vector3(0, 0, -0.5f));
        GetComponent<CircleCollider2D>().isTrigger = false;
        StartCoroutine(Collided());
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!ramasse)
        {
            if (collision.gameObject.tag == "player")
            {
                collision.GetComponent<Character>().AddLoot(value);
                ramasse = true;
                StartCoroutine(End());
            }
        }
        
    }


    IEnumerator Collided()
    {
        yield return new WaitForSeconds(0.05f);
        GetComponent<CircleCollider2D>().isTrigger = true;
    }

    IEnumerator End()
    {
        sounding.Play();
        while (sounding.isPlaying)
        {
            yield return 0;
        }
        Destroy(gameObject);
    }

}
