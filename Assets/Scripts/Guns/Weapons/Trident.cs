﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trident : Gun
{
    GameManager gm;
    List<Vector3> targets= new List<Vector3>();
    public GameObject explosion;
    public LineRenderer[] side;
    public Transform[] sidePos;

    public override void Grabbed2()
    {
        gm = FindObjectOfType<GameManager>();
        foreach (LineRenderer line in side)
        {
            line.startColor = lines[owner];
            line.endColor = lines[owner];
        }
    }

    

    public override void Pattern()
    {
        for (int i = 0; i < 2; i++)
        {
            if (!standing)
            {


                //On récupère le aim du propriétaire ==> le Vector2 du joystick droit
                Vector2 targeting = ownerChar.aim;

                //On prend un point exagérément loin dans ce vecteur, pour avoir une meilleure précision
                var sidemousePos = Camera.main.WorldToScreenPoint(socket.transform.position) + new Vector3(targeting.x, targeting.y, 0) * 1000;

                //On considère l'endroit où se trouve actuellement la socket
                socketPos = Camera.main.WorldToScreenPoint(socket.transform.position);
                //On casse ensuite le vecteur, en prenant en compte la différence de position entre le point visé et la socket
                sidemousePos.x = mousePos.x - socketPos.x;
                sidemousePos.y = mousePos.y - socketPos.y;
                //Puis on calcule l'angle en radian qui en résulte, avant de le repasser en degrés pour avoir une valeur utile entre 180 et -180;
                var sideangle = Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg;
                sideangle += -45 + 90 * i;

                sidemousePos = (Vector2)(Quaternion.Euler(0, 0, sideangle) * Vector2.right);
                //Avec tout ça, on peut donc simuler l'angle et la direction du tir


                //Ensuite, on vérifie si le joueur est bien en train de viser, en regardant qu'on est au dessus de 0.25 dans au moins une direction. Le test est en valeur absolue pour prendre en compte la gauche et le bas
                if (Mathf.Abs(mousePos.x / 1000) > 0.25 || Mathf.Abs(mousePos.y / 1000) > 0.25)
                {
                    //On active le laser
                    side[i].enabled = true;
                    RaycastHit2D hit = Physics2D.Raycast(muzzle.position, new Vector2(sidemousePos.x, sidemousePos.y));
                    //Le point d'arrivée est placé là où le rayon s'arrête
                    sidePos[i].position = hit.point;

                    //On place entre le point de départ du renderer sur le point de tir, et son point d'arrivée sur la position du rayon, pour créer le laser visible
                    side[i].SetPosition(0, muzzle.position);
                    side[i].SetPosition(1, sidePos[i].position);
                }
                //On désactive le laser si le joueur n'est pas activement en train de viser, pour éviter certains pb.
                else
                {
                    if (!lockOn)
                    {
                        side[i].enabled = false;

                    }
                }
            }
            else
            {
                side[i].enabled = false;
                transform.rotation = Quaternion.identity;
            }
        }
    }



    public override void ShootingPattern()
    {

        targets.Clear();

        

        //On réutilise le même système que pour le laser
        Vector2 targeting = ownerChar.aim;
        mousePos = Camera.main.WorldToScreenPoint(socket.transform.position) + new Vector3(targeting.x, targeting.y, 0) * 1000;

        socketPos = Camera.main.WorldToScreenPoint(socket.transform.position);
        mousePos.x = mousePos.x - socketPos.x;
        mousePos.y = mousePos.y - socketPos.y;
        angle = Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg;

        GameObject[] temps = new GameObject[3];
        for (int i = 0; i < 3; i++)
        {
            //On créé l'objet
            temps[i] = Instantiate(projec, muzzle.position, Quaternion.identity);
            
            //On adapte ensuite le tir pour qu'il soit dans l'angle visé
            temps[i].transform.rotation = Quaternion.Euler(0, 0, angle);

            float anguleux = -45 + 45 * i+Random.Range(-maxAngle, maxAngle+1);
            //On rajoute un -90 parce que le truc marche pas sinon. Et on y ajoute un random qui permet de donner de la dispersion.
            temps[i].transform.Rotate(new Vector3(0, 0, anguleux));
            temps[i].GetComponent<TridentRock>().SetTrident(this, anguleux + angle);
            temps[i].GetComponent<Projectile>().Create(owner);
            
        }

        
    }


    public void AddTarget(Vector3 pos)
    {
        targets.Add(pos);
        if (targets.Count >= 3)
        {
            Vector3 locked = Vector3.zero;
            locked.z = transform.position.z;
            float x = 0;
            float y = 0;
            for (int i = 0; i < targets.Count; i++)
            {
                x += targets[i].x;
                y += targets[i].y;
            }
            x += transform.position.x;
            y += transform.position.y;
            x /= 4;
            y /= 4;
            locked.x = x;
            locked.y = y;
            StartCoroutine(Explosion(locked));
        }
    }

    IEnumerator Explosion(Vector3 explo)
    {
        yield return new WaitForSeconds(0.5f);
        
        gm.Shake(1, 30, 1.05f);
        yield return new WaitForSeconds(1.5f);
        GameObject temp=Instantiate(explosion, explo, Quaternion.identity);
        temp.GetComponent<Projectile>().Create(owner);
        yield return new WaitForSeconds(1.5f);
        Visual();
    }


}
