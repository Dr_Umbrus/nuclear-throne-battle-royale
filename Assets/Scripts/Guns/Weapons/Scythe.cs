﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scythe : Gun
{
    bool co = false;

    public override void ShootingPattern()
    {
        if(!co)
        StartCoroutine(ShootyMode());
    }

    IEnumerator ShootyMode()
    {
        co = true;
        ownerChar.speed /= 2;
        for (int i = 0; i < 5; i++)
        {

            yield return new WaitForSeconds(0.25f);
        }
        ownerChar.speed *=2;
        co = false;
    }
}
