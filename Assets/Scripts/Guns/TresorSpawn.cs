﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TresorSpawn : MonoBehaviour
{
    public GameObject treasure;

    float timer;
    public float baseTime;
    public Sprite spawnSprite;

    bool spawning = false;
    SpriteRenderer sr;
    Animator anim;


    private void Awake()
    {
        anim = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
        timer = baseTime + Random.Range(-15, 36) / 10;
    }
    private void Update()
    {
        if (!spawning)
        {
            timer -= Time.deltaTime;
        }
            

            if (timer <= 0 && !spawning)
            {
            spawning = true;
            anim.SetTrigger("spawning");
            }

            if(sr.sprite==spawnSprite && spawning)
        {
            SummonTreasure();
        }
    }

    public void SummonTreasure()
    {
        timer = baseTime + Random.Range(-15, 36) / 10;
        spawning = false;
        Vector2 pos = new Vector2(transform.position.x, transform.position.y);
        GameObject temp = Instantiate(treasure, pos + Random.insideUnitCircle * 2.5f, Quaternion.identity);
    }
}
