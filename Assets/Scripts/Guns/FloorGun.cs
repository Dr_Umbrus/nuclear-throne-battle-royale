﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorGun : Floored
{
    //Arme random par terre

    //La liste des armes (note : ajouter un weight quand on aura plus de contenu ?)
    public GameObject[] guns;

    //La fonction quand on récupère l'objet au sol
    public override void GrabGun(Character chara)
    {
        //On décide au hasard quoi créer
        int rand = Random.Range(0, guns.Length);
        //Puis on le créé et on active le grabMe;
        GameObject temp = Instantiate(guns[rand], transform.position, Quaternion.identity);
        temp.GetComponent<Gun>().GrabMe(chara);
        //Et on détruit le préfab au sol
        Destroy(gameObject);
    }

    

}
