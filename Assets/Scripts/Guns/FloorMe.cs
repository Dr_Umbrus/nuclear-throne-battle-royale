﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorMe : Floored
{
    //Arme posée au sol après un swap

        //Quelle arme c'est
    public GameObject gun;


    private void Awake()
    {
        //4 secondes avant destruction pour ne pas avoir des armes partout par terre.
        StartCoroutine(Dying());
    }

    //La fonction quand on récupère l'objet au sol
    public override void GrabGun(Character chara)
    {
        //Invoque l'arme, et active sa fonction grabMe, puis détruit le prefab au sol
        GameObject temp = Instantiate(gun, transform.position, Quaternion.identity);
        temp.GetComponent<Gun>().GrabMe(chara);
        if (spawn != null)
        {
            spawn.Grabbing();
        }
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        
    }

    IEnumerator Dying()
    {
        yield return new WaitForSeconds(8);
        if (spawn != null)
        {
            spawn.Grabbing();
        }
        yield return 0;
        Destroy(gameObject);
    }

}
