﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    public Teleporter partner;
    public List<GameObject> traverse;
    public bool sideTP;
    

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject col = collision.gameObject;
        if (collision.GetComponent<BoomerangShot>() == null)
        {
            bool test = false;

            if (traverse.Count > 0)
            {
                
                for (int i = 0; i < traverse.Count; i++)
                {
                    if (traverse[i] == col)
                    {
                        test = true;
                    }
                }
            }
            if (!test)
            {
                partner.traverse.Add(col);
                traverse.Add(col);
                StartCoroutine(WorkAgain(col));
                if (sideTP)
                {
                    col.transform.position = new Vector3(partner.transform.position.x,col.transform.position.y, col.transform.position.z);
                }
                else
                {
                    col.transform.position = new Vector3(col.transform.position.x, partner.transform.position.y, col.transform.position.z);
                }

                if (collision.GetComponent<TrailRenderer>() != null)
                {
                    collision.GetComponent<TrailRenderer>().Clear();
                }
            }
        }
    }

    IEnumerator WorkAgain(GameObject target)
    {
        yield return new WaitForSeconds(1f);
        traverse.Remove(target);
        partner.traverse.Remove(target);
    }
}
