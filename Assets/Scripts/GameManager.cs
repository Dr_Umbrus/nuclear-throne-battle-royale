﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;




public class GameManager : MonoBehaviour
{
    public bool deathmatch = false;

    
    //Singleton
    private static GameManager _instance;

    public static GameManager Instance { get { return _instance; } }

    //Le truc de l'UI
    StatsManager stat = null;

    //Combien de joueurs
    public int nPlayer;

    public int[] scores;

    //Le temps règlementaire (note : ajouter où le paramétrer)
    public float startTime;

    //Si on est en jeu ou pas
    public bool inGame;

    //Le score à atteindre
    public int targetScore;

    //La liste des persos du jeu
    public GameObject[] characters;

    //La liste des id des personnages choisis (pr rapport à la liste précédente)
    public int[] chosenCharacters;

    //Les personnages une fois créés
    public Character[] team;

    //La caméra à shaker
    public ScreenShake shaked;

    public Rideau rid;

    public GameObject treasuresPrefab, bigTres;

    public PlayerInput[] playerInputs;

    public int nextLevel = 0;

    public GameObject[] particules;

    bool loadingBattle = false;
    //Singleton
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        DontDestroyOnLoad(gameObject);

        
    }

    private void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.O))
        {
            /*for (int i = 0; i < nPlayer; i++)
            {
                chosenCharacters[i] = 0;
            }*/
            CharacterSelected();
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            BackMenu();
        }

        if (Input.GetKeyDown(KeyCode.I))
        {
            EndGame();
        }


    }


    //Invocation des joueurs
    public void Summon()
    {
        for(int i=0; i<nPlayer; i++)
        {
            if (chosenCharacters[i] >= 0)
            {
                //On créé les joueurs, on leur donne leurs stats, et on les ajoute à team
               GameObject temp= Instantiate(characters[chosenCharacters[i]]);
                temp.GetComponent<Character>().SetStats(i);
                team[i]=temp.GetComponent<Character>();
                temp.GetComponent<Character>().paillettes = particules[i];
            }
            AddBoss();
        }

        


    }

    public void AddBoss()
    {
        /*GameObject temp = Instantiate(bosses[nextLevel], Vector3.zero, Quaternion.identity);
        temp.GetComponent<Boss>().SetStats(this);
        boss = temp.GetComponent<Boss>();*/

        //Et on lui lance sa fonction avec les joueurs
        stat.SelectPlayers(team);

    }

        
    //Quand quelqu'un fait un kill
    public void Kill(int id)
    {
        if (id >= 0)
        {
            team[id].killStreak++;
            if (deathmatch)
            {

                team[id].kills++;
                scores[id]++;
                if (scores[id] >= targetScore)
                {
                    EndGame();
                }
            }
        }
        
        /*if (boss != null)
        {
            boss.targetDeath(id);
        }*/
        
    }

    public void PurgeStats()
    {
        for(int i=0; i<4; i++)
        {
            scores[i] = 0;
        }
        
    }

    //Retour au menu
    public void BackMenu()
    {
        SceneManager.LoadScene("SampleScene");
        PurgeStats();
    }

    public void CharacterSelected()
    {
        StartCoroutine(ChangeScene("ChoixNiveau"));
    }

    //Les boutons pour lancer le jeu (placeholder)
    public void ButtonBattle(int nb)
    {
        //Déterminent un nb de joueurs et lancent le jeu
        nPlayer = 2;
        for (int i = 0; i < team.Length; i++)
        {
            team[i] = null;
            chosenCharacters[i] = -1;
        }
        StartCoroutine(ChangeScene("CharacterScene"));
    }

    //Fin de la partie, on se débarasse de l'UI et on lance la séquence de fin
    public void EndGame()
    {
        for (int i = 0; i < nPlayer; i++)
        {
            scores[i] = team[i].AskScore();
        }
        //boss = null;
        for (int i = 0; i < nPlayer; i++)
        {
            team[i].end = true;
        }
        StartCoroutine(ChangeScene("ResultScreen"));
    }

    

    //Début du combat
    public void StartBattle()
    {
        loadingBattle = true;
        StartCoroutine(ChangeScene("Scene Combat"+nextLevel));
    }


    //Fonction à appeler si quelqu'un veut lancer un screenshake
    public void Shake(float dura, float str, float dank)
    {
        shaked.StartShaking(dura, str, dank);
    }

    public void SummonTreasure(Vector3 posi)
    {
        Vector2 pose= Random.insideUnitCircle*4;
        

        
        Vector3 pos = new Vector3(pose.x, pose.y, 0);
        Instantiate(treasuresPrefab, posi+pos, Quaternion.identity);
    }

    public void SummonBigTreasure(Vector3 posi, int target)
    {

       GameObject temp=Instantiate(bigTres, posi, Quaternion.identity);
        temp.GetComponent<BigTreasure>().SetTarget(target);
    }



    //Mise en place du jeu
    IEnumerator PrepareThings()
    {
        //Une frame de pause pour qu'on ait bien changé de scène
        yield return 0;
        //On récupère les trucs importants
        stat = FindObjectOfType<StatsManager>();
        shaked = FindObjectOfType<ScreenShake>();
        inGame = true;
        //On pose les joueurs
        Summon();

        
        //On attend d'avoir bien récupéré le stat, donc on attend tant que c'est pas bon
        while (stat == null)
        {
            yield return 0;
        }
      
    }

    IEnumerator ChangeScene(string futureScene)
    {
        rid.Close();
        while (!rid.sent)
        {
            yield return 0;
        }
        SceneManager.LoadScene(futureScene);
        yield return 0;
        rid.Open();

        if(loadingBattle)
        {
            StartCoroutine(PrepareThings());
        }
        else
        {
            inGame = false;
            stat = null;
        }
        loadingBattle = false;
    }

}
