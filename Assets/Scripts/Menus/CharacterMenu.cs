﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterMenu : MonoBehaviour
{
    //Changer l'affichage du perso

    Image perso;
    public Sprite nextCharacter=null;
    public Vector3 posTop, postBot;
    public float timer=0;

    private void Awake()
    {
        perso = GetComponent<Image>();
        posTop = transform.position;
        postBot = new Vector3(transform.position.x, transform.position.y - 850, transform.position.z);
        transform.position = postBot;
        nextCharacter = perso.sprite;
        perso.color = Color.clear;
    }

    private void FixedUpdate()
    {


        if (perso.sprite != nextCharacter)
        {
            timer -= Time.deltaTime * 2.5f;
        }
        else
        {
            timer += Time.deltaTime * 2.5f;
        }
        if (timer > 1)
        {
            timer = 1;
        }
        if (timer < 0)
        {
            timer = 0;
        }

        transform.position = Vector3.Lerp(postBot, posTop, timer);
        if (timer <= 0)
        {
            perso.sprite = nextCharacter;
            if (nextCharacter == null)
            {
                perso.color = Color.clear;

            }
            else
            {
                perso.color = Color.white;
            }
        }
    }

    public void ChangeSprite(Sprite next)
    {
        nextCharacter = next;

    }
}
