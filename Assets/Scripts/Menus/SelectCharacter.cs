﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class SelectCharacter : MonoBehaviour
{
    GameManager gm;
    public int pos = 0;
    int oldpos;
    public Transform[] positions;
    public bool move = false;
    public bool selected=true;
    public int id;
    public GameObject text;
     Image image;
    public Color startColor, endColor;
    public SummonCursors father;
    Image backGround;
    public GameObject j;


    public void Stats(Transform[] tran, GameObject the, SummonCursors curs, Image bg)
    {
        text = the;
        for (int i = 0; i < tran.Length; i++)
        {
            positions[i] = tran[i];
        }
        image = GetComponent<Image>();
        gm = FindObjectOfType<GameManager>();
        
        pos = id;
        transform.position = positions[pos].position;
        father = curs;
        selected = false;
        if (id > 1)
        {
            //father.KillVisual(id);
            image.color = Color.clear;
            backGround = bg;
            backGround.color = Color.grey;
            father.ChangeCharac(-1, id);
            j.SetActive(false);
        }
        else
        {
            
            father.ChangeCharac(pos, id);
        }
    }

    public void OnMove(InputValue value)
    {
        if (!selected && image.color != Color.clear)
        {
            oldpos = pos;
            Vector2 potato = value.Get<Vector2>();
            if (!move)
            {
                if (potato.y > 0.5f || potato.y < -0.5f)
                {
                    if (pos == 0)
                    {
                        pos = 2;
                    }
                    else if (pos == 2)
                    {
                        pos = 0;
                    }
                    else if (pos == 1)
                    {
                        pos = 3;
                    }
                    else if (pos == 3)
                    {
                        pos = 1;
                    }
                    move = true;
                }
                if (potato.x > 0.5f)
                {
                    if (pos < 3)
                    {
                        pos++;
                    }
                    else
                    {
                        pos = 0;
                    }
                    move = true;
                }
                else if (potato.x < -0.5f)
                {
                    if (pos > 0)
                    {
                        pos--;
                    }
                    else
                    {
                        pos = 3;
                    }
                    move = true;
                }
            }
            else if (potato.x > -0.2f && potato.x < 0.2f && potato.y < 0.2f && potato.y > -0.2f)
            {
                move = false;

            }
            if (pos != oldpos)
            {
                father.ChangeCharac(pos, id);
            }
        }
        

        transform.position = positions[pos].position;
    }

    public void OnGrab()
    {
        if(image.color!=Color.clear)
        {

        
        selected = true;
        
        image.color = endColor;
        gm.chosenCharacters[id] = pos;
        bool test = true;
        for (int i = 0; i < gm.nPlayer; i++)
        {
            if (gm.chosenCharacters[i] < 0)
            {
                test = false;
            }
        }

        if (test)
        {
            text.gameObject.SetActive(true);
        }
        }

        else
        {
            image.color = startColor;
            backGround.color = startColor;
            gm.nPlayer++;
            pos = id;
            transform.position = positions[pos].position;
            j.SetActive(true);
            text.gameObject.SetActive(false);
        }
    }

    public void OnBack()
    {
            selected = false;
            image.color = startColor;
            bool test = true;
            for (int i = 0; i < gm.nPlayer; i++)
            {
                if (gm.chosenCharacters[i] >= 0)
                {
                    test = false;
                }
            }
            if (test)
            {
                gm.BackMenu();
            }

            if (gm.chosenCharacters[id] >= 0)
            {
                gm.chosenCharacters[id] = -1;
            }
        
    }

    public void OnPause()
    {
        if (text.activeSelf)
        {
            gm.CharacterSelected();
        }
    }
}
