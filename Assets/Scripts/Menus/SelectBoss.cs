﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class SelectBoss : MonoBehaviour
{
    GameManager gm;
    public int pos = 0;
    public Transform[] positions;
    public bool move = false;


    private void Awake()
    {
        gm = FindObjectOfType<GameManager>();
        transform.position = positions[0].position;
    }
    public void OnMove(InputValue value)
    {
            Vector2 potato = value.Get<Vector2>();
            if (!move)
            {
                if (potato.y > 0.5f || potato.y < -0.5f)
                {
                    if (pos == 0)
                    {
                        pos = 2;
                    }
                    else if (pos == 2)
                    {
                        pos = 0;
                    }
                    else if (pos == 1)
                    {
                        pos = 3;
                    }
                    else if (pos == 3)
                    {
                        pos = 1;
                    }
                    move = true;
                }
                if (potato.x > 0.5f)
                {
                    if (pos < 3)
                    {
                        pos++;
                    }
                    else
                    {
                        pos = 0;
                    }
                    move = true;
                }
                else if (potato.x < -0.5f)
                {
                    if (pos > 0)
                    {
                        pos--;
                    }
                    else
                    {
                        pos = 3;
                    }
                    move = true;
                }
            }
            else if (potato.x > -0.2f && potato.x < 0.2f && potato.y < 0.2f && potato.y > -0.2f)
            {
                move = false;

        }
        

        transform.position = positions[pos].position;
    }

    public void OnGrab()
    {

        gm.nextLevel = pos;

        if(pos%2==0)
        {
            gm.deathmatch = false;
            
        }
        else
        {
            gm.deathmatch = true;
            gm.targetScore = 10;
        }
        gm.StartBattle();
    }

    public void OnBack()
    {
            gm.BackMenu();

        
    }
}
