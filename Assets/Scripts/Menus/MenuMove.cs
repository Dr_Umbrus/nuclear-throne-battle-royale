﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class MenuMove : MonoBehaviour
{
    GameManager gm;
    int pos = 0;
    public Transform[] positions;
    bool move=false;

    private void Awake()
    {
        gm = FindObjectOfType<GameManager>();
        transform.position = positions[pos].position;
    }

    public void OnMove(InputValue value)
    {
        Vector2 potato = value.Get<Vector2>();
        if (!move)
        {
            if (potato.x > 0.5f)
            {
                if (pos < 2)
                {
                    pos++;
                }
                else
                {
                    pos = 0;
                }
                move = true;
            }
            else if (potato.x < -0.5f)
            {
                if (pos > 0)
                {
                    pos--;
                }
                else
                {
                    pos = 2;
                }
                move = true;
            }
        }
        else if (potato.x >-0.1f && potato.x < 0.1f)
        {
            move = false;

        }

        transform.position = positions[pos].position;
    }

    public void OnGrab()
    {
        gm.ButtonBattle(pos+2);
    }


}
