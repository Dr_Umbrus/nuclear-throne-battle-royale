﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SummonCursors : MonoBehaviour
{
    public Transform[] positions0;
    public Transform[] positions1;
    public Transform[] positions2;
    public Transform[] positions3;
    public GameObject[] prefabs;
    public GameObject the;
    public Canvas can;
    public CharacterMenu[] select;
    public Sprite[] characters;
    public Image[] backgrounds;

    private void Awake()
    {
        StartCoroutine(Create());
    }

    IEnumerator Create()
    {
        yield return 0;
        for (int i = 0; i < prefabs.Length; i++)
        {
            GameObject temp = Instantiate(prefabs[i], transform.position, Quaternion.identity, can.transform);
            if (i == 0)
            {
                temp.GetComponent<SelectCharacter>().Stats(positions0, the, this, backgrounds[i]);
            }
            else if (i == 1)
            {
                temp.GetComponent<SelectCharacter>().Stats(positions1, the, this, backgrounds[i]);
            }
            else if (i == 2)
            {
                temp.GetComponent<SelectCharacter>().Stats(positions2, the, this, backgrounds[i]);
            }
            else if (i == 3)
            {
                temp.GetComponent<SelectCharacter>().Stats(positions3, the, this, backgrounds[i]);
            }

            yield return 0;
        }
    }

    public void ChangeCharac(int sprite, int which)
    {
        if (sprite>= 0)
        {
            select[which].ChangeSprite(characters[sprite]);
        }
        
        else
        {
            select[which].ChangeSprite(null);
        }
    }

    public void KillVisual(int id)
    {
        select[id].gameObject.SetActive(false);
    }
}
