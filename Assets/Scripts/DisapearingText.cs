﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisapearingText : MonoBehaviour
{
    public float speedDiv;
    Text text;
    float timer = 1;
    public Color cle, op;


    private void Awake()
    {
        timer = 1;
    }
    private void Update()
    {
        timer -= Time.deltaTime / speedDiv;

        text.color = Color.Lerp(cle, op, timer);
        if (timer <= 0)
        {
            gameObject.SetActive(false);
        }
    }
}
