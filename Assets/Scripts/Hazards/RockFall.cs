﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockFall : MonoBehaviour
{
    CircleCollider2D box;
    public int damage;
    public Sprite activation, destruction;
    SpriteRenderer sr;
    public int ignore = -1;
    public float timeFall=1.5f;
    public bool check = false;


    private void Awake()
    {
        box = GetComponent<CircleCollider2D>();
        sr = GetComponent<SpriteRenderer>();
        StartCoroutine(WaitFall());
        box.enabled = false;
    }

    private void Update()
    {
        if (check)
        {
            Debug.Log("pos : " + transform.position);
            Debug.Log("sprite : " + sr.sprite);
            Debug.Log("box : " + box.enabled);
        }
        if (sr.sprite == activation)
        {
            box.enabled = true;
        }
        else if (sr.sprite == destruction)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "player")
        {
            if(collision.GetComponent<Character>().playerNum!=ignore)
            collision.GetComponent<Character>().Damage(damage, -1);
        }
    }

    IEnumerator WaitFall()
    {
        yield return new WaitForSeconds(timeFall);
        GetComponent<Animator>().SetTrigger("fall");
    }
}
