﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthQuake : MonoBehaviour
{
    GameManager gm;
    public GameObject shadow;
    public float timer;
    public float maxTimer;
    public int variante;
    public float arenaSizeY, arenaSizeX;


    private void Awake()
    {
        timer = maxTimer;
        gm = FindObjectOfType<GameManager>();

    }

    private void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            timer = 999;
            StartCoroutine(Quaking());
        }
    }

    IEnumerator Quaking()
    {
        int rocks = Random.Range(5,11);

        gm.Shake(5, 35, 1.05f);
        //gm.Shake(rocks * 0.25f, 4, 0.2f);
        for (int i = 0; i < rocks; i++)
        {
            Vector3 pos = Vector3.zero;
            pos.z = -2;
            float phi=Random.Range(0, 2 * Mathf.PI);
            float rho = Random.value;
            pos.x = Mathf.Sqrt(rho) * Mathf.Cos(phi)*arenaSizeX;
            pos.y = Mathf.Sqrt(rho) * Mathf.Sin(phi)*arenaSizeY;
            pos.y += 3;
            Instantiate(shadow, transform.position+pos, Quaternion.identity);
            yield return new WaitForSeconds(Random.Range(0.25f,1.25f));
        }

        timer = maxTimer + Random.Range(-variante, variante + 1) / 10;
    }
}
