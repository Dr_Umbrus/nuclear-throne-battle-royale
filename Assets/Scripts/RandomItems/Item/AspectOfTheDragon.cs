﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AspectOfTheDragon : MonoBehaviour
{
    Character cara;
    SpriteRenderer sr;
    bool dying=false;
    float timer = 1;
    Color cle, op;
    public int damage;
    int num;
    public int angle;

    public void SetStats(Character cha)
    {
        cara = cha;
        num = cha.playerNum;
        sr = GetComponent<SpriteRenderer>();
        cle = cha.playerCol[num];
        op = new Color(cle.r, cle.g, cle.b, 0);
        cara.gun.multiPattern *= 2;
        cara.gun.maxAngle += angle;
        cara.gun.multishot *= 2;
    }

    void Update()
    {
        if (dying)
        {
            timer -= Time.deltaTime * 1.5f;
            sr.color = Color.Lerp(cle, op, timer);

            if (timer <= 0)
            {
                Destroy(gameObject);
            }
            
        }
    }

    IEnumerator Aspect()
    {

        yield return new WaitForSeconds(5);
        dying = true;
        cara.gun.multiPattern /= 2;
        cara.gun.maxAngle -= angle;
        cara.gun.multishot /= 2;
    }
}
