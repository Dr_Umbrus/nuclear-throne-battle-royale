﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AspectOfTheHopper : MonoBehaviour
{
    Character cara;
    CircleCollider2D cc;
    SpriteRenderer sr;
    bool dying=false;
    float timer = 1;
    Color cle, op;
    public int damage;
    int num;
    public float speed;

    public void SetStats(Character cha)
    {
        cara = cha;
        num = cha.playerNum;
        cc.enabled=true;
        sr = GetComponent<SpriteRenderer>();
        cle = cha.playerCol[num];
        op = new Color(cle.r, cle.g, cle.b, 0);
        speed = cara.baseSpeed + speed;
        
        
    }

    void Update()
    {
        transform.position = cara.transform.position;
        if (dying)
        {
            timer -= Time.deltaTime * 1.5f;
            sr.color = Color.Lerp(cle, op, timer);

            if (timer <= 0)
            {
                Destroy(gameObject);
            }
            
        }

        if (cara.speed < speed)
        {
            cara.speed = speed;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "player" && collision.gameObject!=cara.gameObject)
        {
            collision.GetComponent<Character>().Damage(damage, num);
        }
    }

    IEnumerator Aspect()
    {

        yield return new WaitForSeconds(10);
        cc.enabled = false;
        dying = true;
    }

    private void OnDestroy()
    {
        cara.speed = cara.baseSpeed;
    }
}
