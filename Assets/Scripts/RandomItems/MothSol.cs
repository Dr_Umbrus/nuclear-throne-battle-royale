﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MothSol : MonoBehaviour
{
    GameObject aspect;
    bool ramasse = false;
    public AudioSource sounding;
    public RandomSpawner rs;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!ramasse)
        {
            if (collision.gameObject.tag == "player")
            {
                GameObject temp = Instantiate(aspect, transform.position, Quaternion.identity);

                ramasse = true;
                StartCoroutine(End());
                rs.Grabbing();
            }
        }

    }

    IEnumerator End()
    {
        sounding.Play();
        while (sounding.isPlaying)
        {
            yield return 0;
        }
        Destroy(gameObject);
    }
}
