﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{
    public GameObject wall;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "player")
        {
            StartCoroutine(OpenWall());
        }
        
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "player")
        {
            StartCoroutine(OpenWall());
        }
    }

    IEnumerator OpenWall()
    {
        if (wall.activeSelf)
        {
            wall.SetActive(false);
            yield return new WaitForSeconds(1.5f);
            wall.SetActive(true);
        }

    }
}
