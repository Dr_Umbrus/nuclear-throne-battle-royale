﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolarFlare : MonoBehaviour
{
    public int damage;
    public List<SafeZone> haven;
    public List<Character> team;
    BossBeetle creator;
    bool active = false;
    bool dying=false;
    public float timeActive;
    SpriteRenderer sr;
    Color activeCol= new Color(1, 0, 0, 0.5f);

    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        //creator = FindObjectOfType<BossBeetle>();
        transform.localScale = Vector3.zero;
        StartCoroutine(AddStats());
    }

    IEnumerator AddStats()
    {
        yield return 0;
        SafeZone[] safe = FindObjectsOfType<SafeZone>();

        Character[] cha= FindObjectsOfType<Character>();
        while (safe.Length <= 0 || cha.Length<=0)
        {
            cha = FindObjectsOfType<Character>();
            yield return 0;
        }
        foreach (SafeZone saf in safe)
        {
            haven.Add(saf);
        }

        
        foreach (Character c in cha)
        {
            team.Add(c);
        }
    }

    private void Update()
    {
        if (transform.localScale.x < 50)
        {
              transform.localScale = transform.localScale + Vector3.one * Time.deltaTime * 25;
        }
        else
        {
            if (!active)
            {
                active = true;
                sr.color = activeCol;
                StartCoroutine(Damaging());
            }
        }

        if (active)
        {
            if (timeActive > 0)
            {
                timeActive -= Time.deltaTime;   
            }
            else
            {
                //creator.EndAttack();
                dying=true;
                active = false;
                timeActive = 1;
            }
        }

        if (dying)
        {
            timeActive -= Time.deltaTime;
            sr.color = Color.Lerp(Color.clear, activeCol, timeActive);

            if (sr.color.a <= 0.1f)
            {
                Destroy(gameObject);
            }
        }
    }

    IEnumerator Damaging()
    {
        for (int i = 0; i < team.Count; i++)
        {
            bool safe = false;
            for (int j = 0; j < haven.Count; j++)
            {
                if (Vector2.Distance(team[i].transform.position, haven[j].transform.position) < haven[j].range)
                {
                    safe = true;
                }
            }
            if (!safe)
            {
                team[i].Damage(damage, -1);
            }
        }

        yield return new WaitForSeconds(0.5f);
        if (active)
        {
            StartCoroutine(Damaging());
        }
    }
}
