﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossBeetle : Boss
{
    public Image[] lifes;
    int[] angry = new int[4];
    int angryTresh;
    bool buffer = false;
    public int chance1;
    public bool dashing = false;
    Vector2 dashDir= Vector2.zero;
    public GameObject flare;

    public override void Pattern()
    {
        if (dashing)
        {
            transform.Translate(dashDir * speed * 2 * Time.deltaTime);

        }
        if (!stunned && !stopped && !dashing)
        {
            Vector2 target = Vector2.zero - new Vector2(transform.position.x, transform.position.y);
            target.Normalize();

            //Puis il va vers elle
            transform.Translate(target * speed * Time.deltaTime);

            if (timeAttack > 0)
                {
                    timeAttack -= Time.deltaTime;
                }

            if (timeAttack <= 0)
            {
                if (!loading && !ready)
                {
                    loading = true;
                    StartCoroutine(Load());
                }


            }

            //Quand on a réussi un check et qu'on a pas encore commencé à attaquer
            if (ready && !attacking)
            {
                //On arrête de bouger
                stopped = true;

                //Et on passe en mode attaque
                attacking = true;
                ready = true;

                //On tire au hasard l'attaque à utiliser, avec un cumul de proba de 100%
                int rand = Random.Range(0, 100);

                //On lance l'attaque associée au résultat. Note : L'utilisation de variable permet de changer les probas entre les phases ou entre les parties.
                if (rand < chance1)
                {
                    loadingattack = 1;
                    sr.color = Color.green;
                    StartCoroutine(WaitFlare());
                }
                else
                {
                    loadingattack = 2;
                    sr.color = Color.blue;
                }

                //StartCoroutine(Attack());
            }

            if(buffer && !attacking && !dashing)
            {
                stopped=true;
                buffer = false;
            }
        }
    }

    IEnumerator PrepareDash()
    {
        sr.color = Color.black;
        yield return new WaitForSeconds(1.25f);
        dashDir = aggro.transform.position - transform.position;
        dashDir.Normalize();
        dashing = true;
    }

    public override void Damaged(int damage, int owner)
    {
        if (vulnerable)
        {
            audi.Play();
            //Enlever de la vie
            life -= damage;
            if (phase < threshold.Length)
            {
                lifebar.value = life - threshold[phase];
            }
            else
            {
                lifebar.value = life;
            }


            //Les dégâts sont ajoutés au décompte.
            damages[owner] += damage;

            //Préparer la mort du boss s'il est à 0hp
            if (life <= 0)
            {
                var gune = Random.Range(1, 4) + phase;
                for (int j = 0; j < gune * 2; j++)
                {
                    Vector2 pose = Random.insideUnitCircle * 2.5f;
                    Vector3 pos = new Vector3(pose.x, pose.y, 0);
                    Instantiate(treasure, transform.position + pos, Quaternion.identity);
                    /*Vector2 pose2 = Random.insideUnitCircle * 2.5f;
                    Vector3 pos2 = new Vector3(pose2.x, pose2.y, 0);
                    Instantiate(gun, transform.position + pos2, Quaternion.identity);*/
                }

                team[owner].GetComponent<Character>().AddLoot(5);
                team[owner].GetComponent<Character>().AddLoot(5);
                StartCoroutine(Dying());
            }
            else
            {
                //Il aggro le joueur qui a fait le plus de dégâts.
                int potato = -1;
                for (int i = 0; i < gm.nPlayer; i++)
                {
                    if (damages[i] > potato)
                    {
                        potato = damages[i];
                        aggro = team[i];
                        targetAggro = i;
                    }
                }

                //On vérifie si sa vie est assez basse pour changer de phase.
                for (int i = 0; i < threshold.Length; i++)
                {
                    if (phase <= i && life < threshold[i])
                    {
                        StopCoroutine(Vulnerable());
                        StartCoroutine(Stun());
                        phase++;
                        for (int j = 0; j < lifes.Length; j++)
                        {
                            if (j >= lifes.Length - phase)
                            {
                                lifes[j].gameObject.SetActive(false);
                            }
                        }
                        if (phase < threshold.Length)
                        {
                            lifebar.maxValue = threshold[phase - 1] - threshold[phase];
                            lifebar.value = life - threshold[phase];
                        }
                        else
                        {
                            lifebar.maxValue = threshold[threshold.Length - 1];
                            lifebar.value = life;
                        }


                        var gune = Random.Range(1, 4) + phase;
                        for (int j = 0; j < gune * 2; j++)
                        {
                            Vector2 pose = Random.insideUnitCircle * 3f;
                            Vector3 pos = new Vector3(pose.x, pose.y, 0);
                            Instantiate(treasure, transform.position + pos, Quaternion.identity);

                        }
                        Vector2 pose2 = Random.insideUnitCircle * 3f;
                        Vector3 pos2 = new Vector3(pose2.x, pose2.y, 0);
                        Instantiate(gun, transform.position + pos2, Quaternion.identity);

                        team[owner].GetComponent<Character>().AddLoot(5);
                        vulnerable = false;
                    }
                }
            }
        }

        else
        {
            angry[owner] += damage;
            if (angry[owner] > angryTresh)
            {
                aggro = team[owner];
                targetAggro = owner;
                buffer = true;
            }
        }

    }

    void SolarFlare()
    {
        Instantiate(flare, new Vector3(0, 0, -2), Quaternion.identity);
        attacking = false;
    }

    IEnumerator WaitFlare()
    {

        yield return new WaitForSeconds(0.5f);
        SolarFlare();
    }

    public void BecomeVulnerable()
    {
        stunned = true;
        vulnerable = true;
        StartCoroutine(Vulnerable());
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "solid")
        {

            StartCoroutine(RecoveryWall());
        }
    }

    public void EndAttack()
    {
        stopped = false;
    }

    IEnumerator RecoveryWall()
    {
        dashing = false;
        stopped = true;
        yield return new WaitForSeconds(0.5f);
        stopped = false;
    }

    IEnumerator Vulnerable()
    {
        yield return new WaitForSeconds(4);
        stunned=false;
        stopped = false;
        ready = false;
        attacking = false;
    }
}
