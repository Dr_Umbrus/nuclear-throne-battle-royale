﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressurePlate : MonoBehaviour
{
    public PressurePlate adjacent1, adjacent2;
    public bool activated;
    GameObject onIt=null;
    public Animator anim;
    public ElecWall wall1, wall2;
    

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!activated)
        {
            if (collision.gameObject.tag == "player")
            {
                GetComponent<SpriteRenderer>().color = Color.red;
                //anim.SetTrigger("lit");
                onIt = collision.gameObject;
                activated = true;

                if (adjacent1.activated)
                {
                    wall1.Activate();
                }
                if (adjacent2.activated)
                {
                    wall2.Activate();
                }
            }
        }
        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (onIt != null)
        {
            if (collision.gameObject == onIt)
            {
                onIt = null;
                activated = false;
                wall1.Deactivate();
                wall2.Deactivate();
                GetComponent<SpriteRenderer>().color = Color.white;
            }
        }
    }
}
