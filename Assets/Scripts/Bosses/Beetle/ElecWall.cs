﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElecWall : MonoBehaviour
{
    bool active = false;
    BoxCollider2D col;
    SpriteRenderer sr;
    float timer = 0;

    private void Awake()
    {
        col = GetComponent<BoxCollider2D>();
        sr = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if (!active)
        {
            if (timer > 0)
            {
                timer -= Time.deltaTime;
            }
        }
        else
        {
            if (timer < 1)
            {
                timer += Time.deltaTime;
            }
        }

        sr.color = Color.Lerp(Color.clear, Color.blue, timer);

        if (timer > 0.75)
        {
            col.enabled = true;
        }
        else
        {
            col.enabled = false;
        }
    }

    public void Activate()
    {
        active = true;
    }

    public void Deactivate()
    {
        active = false;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        var coli = collision.gameObject;

        if (coli.tag == "boss")
        {
            var beetle = coli.GetComponent<BossBeetle>();

            if((beetle.dashing || beetle.stopped) && !beetle.stunned)
            {
                beetle.BecomeVulnerable();
            }
        }
    }
}
