﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weakpoint : MonoBehaviour
{

    public float timeToHit;
    public BossMantis mantis;
    public bool triggered;
    public Weakpoint[] bruh;
    public Color narmol, hit;
    SpriteRenderer sr;

    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
    }


    public void Triggered()
    {
        if (!triggered)
        {
            bool test = true;
            foreach(Weakpoint bru in bruh)
            {
                if (!bru.triggered)
                {
                    test = false;
                }
            }

            if (test)
            {
                mantis.BecomeVulnerable();
            }
            else
            {
                triggered = true;
                StartCoroutine(Hitted());
            }
        }
    }

    IEnumerator Hitted()
    {
        sr.color = hit;
        yield return new WaitForSeconds(timeToHit);
        triggered = false;
        sr.color = narmol;
    }
}
