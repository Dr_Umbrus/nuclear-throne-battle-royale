﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossMantis : Boss
{

    public GameObject detection;

    BoxCollider2D box;
    public List<Transform> flowers; 

    //Le slash des griffes
    public GameObject claw;

    public GameObject kama;

    public GameObject crystalSol;
    bool crystalin = false;

    //Les sprites de déclenchement d'attaque
    public Sprite clawSprite, crysSprite, kamaSprite;

    //les statistiques pour les attaques
    public int probaClaw, probaCrys, probaKama;

    public bool hidden = false;

    float hiddenThreshold;
    public float maxHiddenThreshold;

    public GameObject[] weaks;
    public Vector3[] pos;

    public Canvas can;
    
    public Image[] lifes;

    public GameObject repousse;


    public override void Starting()
    {
        lifebar.maxValue = life - threshold[0];
        lifebar.value = lifebar.maxValue;
        box = GetComponent<BoxCollider2D>();
        GameObject[] potato = GameObject.FindGameObjectsWithTag("flower");
        for (int i = 0; i < potato.Length; i++)
        {
            flowers.Add(potato[i].transform);
        }
        foreach(GameObject weak in weaks)
        {
            weak.SetActive(false);
        }
        Hide();
    }

    public override void Pattern()
    {
        if (vulnerable && !attacking)
        {
            sr.color = Color.gray;
        }
        
        //Test
        if (Input.GetKeyDown(KeyCode.C))
        {
            StartCoroutine(Clawing());
        }

        if (Input.GetKeyDown(KeyCode.V))
        {
            StartCoroutine(Crystal());
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            Kamaitachi();
        }

        //S'il est pas stun/en train de taper
        if (!stopped && !hidden)
        {
            //Il regarde sa cible
            Vector2 target = aggro.transform.position - transform.position;
            target.Normalize();

            //Puis il va vers elle
            transform.Translate(target * speed * Time.deltaTime);

            //Flip le sprite si nécessaire.
            if (target.x < 0 && !sr.flipX)
            {
                sr.flipX = true;
                
                if(!hidden && !vulnerable)
                {
                    for (int i = 0; i < weaks.Length; i++)
                    {
                        weaks[i].transform.localPosition = new Vector3(-pos[i].x, pos[i].y, pos[i].z);
                    }
                }
            }
            if (target.x > 0 && sr.flipX)
            {
                sr.flipX = false;

            }

            if(!hidden && !vulnerable)
            {
                if (sr.flipX)
                {
                    for (int i = 0; i < weaks.Length; i++)
                    {
                        weaks[i].transform.localPosition = pos[i];
                    }
                }
                else
                {
                    for (int i = 0; i < weaks.Length; i++)
                    {
                        weaks[i].transform.localPosition = new Vector3(-pos[i].x, pos[i].y, pos[i].z);
                    }
                }
            }

            //Cooldown d'attaque
            if (timeAttack > 0)
            {
                timeAttack -= Time.deltaTime;
            }
        }

        //A la fin du timer d'attaque, on commence les checks
        if (timeAttack <= 0)
        {
            if (!loading && !ready)
            {
                loading = true;
                StartCoroutine(Load());
            }

            
        }


        //Quand on a réussi un check et qu'on a pas encore commencé à attaquer
        if (ready && !attacking)
        {
            //On arrête de bouger
            stopped = true;

            //Et on passe en mode attaque
            attacking = true;
            ready = true;

            //On tire au hasard l'attaque à utiliser, avec un cumul de proba de 100%
            int rand = Random.Range(0, 100);

            //On lance l'attaque associée au résultat. Note : L'utilisation de variable permet de changer les probas entre les phases ou entre les parties.
            if (rand < probaClaw)
            {
                loadingattack = 1;
                sr.color = Color.green;

            }
            else if (rand < probaCrys)
            {
                loadingattack = 2;
                sr.color = Color.blue;
            }
            else
            {
                loadingattack = 3;
                sr.color = Color.red;
            }

            StartCoroutine(Attack());
        }

        
        /*
        //Une fois en mode attaque, on vérifiesi l'animation a atteint la bonne frame.
        if (attacking)
        {
            if (loadingattack == 1)
            {
                if (sr.sprite == clawSprite)
                {
                    Clawing();
                }
            }
            else if (loadingattack == 2)
            {
                if(sr.sprite==crysSprite && !crystalin)
                {
                    StartCoroutine(Crystal());
                }
            }
            else if (loadingattack == 3)
            {
                if (sr.sprite == kamaSprite)
                {
                   Kamaitachi();
                }
            }
        }*/
        
    }

    

    public override void Damaged(int damage, int owner)
    {
        if (hidden)
        {
            audi.Play();
            hiddenThreshold -= damage;
            if (hiddenThreshold <= 0)
            {
                RemoveHide();
            }
        }
        else if (vulnerable)
        {
            audi.Play();
            //Enlever de la vie
            life -= damage;
            if (phase < threshold.Length)
            {
                lifebar.value = life - threshold[phase];
            }
            else
            {
                lifebar.value = life;
            }
                

            //Les dégâts sont ajoutés au décompte.
            damages[owner] += damage;

            //Préparer la mort du boss s'il est à 0hp
            if (life <= 0)
            {
                StopCoroutine(Attack());
                repousse.SetActive(false);
                can.gameObject.SetActive(false);
                box.enabled = false;
                var gune = Random.Range(1, 4) + phase;
                for (int j = 0; j < gune*2; j++)
                {
                    Vector2 pose = Random.insideUnitCircle * 2.5f;
                    Vector3 pos = new Vector3(pose.x, pose.y, 0);
                    Instantiate(treasure, transform.position + pos, Quaternion.identity);
                    /*Vector2 pose2 = Random.insideUnitCircle * 2.5f;
                    Vector3 pos2 = new Vector3(pose2.x, pose2.y, 0);
                    Instantiate(gun, transform.position + pos2, Quaternion.identity);*/
                }

                team[owner].GetComponent<Character>().AddLoot(5);
                team[owner].GetComponent<Character>().AddLoot(5);
                StartCoroutine(Dying());
            }
            else
            {
                //Il aggro le joueur qui a fait le plus de dégâts.
                int potato = -1;
                for (int i = 0; i < gm.nPlayer; i++)
                {
                    if (damages[i] > potato)
                    {
                        potato = damages[i];
                        aggro = team[i];
                        targetAggro = i;
                    }
                }

                //On vérifie si sa vie est assez basse pour changer de phase.
                for (int i = 0; i < threshold.Length; i++)
                {
                    if (phase <= i && life < threshold[i])
                    {
                        StopCoroutine(Attack());
                        StartCoroutine(Stun());
                        phase++;
                        for (int j = 0; j < lifes.Length; j++)
                        {
                            if (j >= lifes.Length - phase)
                            {
                                lifes[j].gameObject.SetActive(false);
                            }
                        }
                        if (phase < threshold.Length)
                        {
                            lifebar.maxValue = threshold[phase - 1] - threshold[phase];
                            lifebar.value = life - threshold[phase];
                        }
                        else
                        {
                            lifebar.maxValue = threshold[threshold.Length - 1];
                            lifebar.value = life;
                        }
                        

                        var gune = Random.Range(1, 4) + phase;
                        for (int j = 0; j < gune*2; j++)
                        {
                            Vector2 pose = Random.insideUnitCircle*3f;
                            Vector3 pos = new Vector3(pose.x, pose.y, 0);
                            Instantiate(treasure, transform.position+pos, Quaternion.identity);
                            
                        }
                       /* Vector2 pose2 = Random.insideUnitCircle * 3f;
                            Vector3 pos2 = new Vector3(pose2.x, pose2.y, 0);
                            Instantiate(gun, transform.position + pos2, Quaternion.identity);*/

                        team[owner].GetComponent<Character>().AddLoot(5);
                        box.enabled = false;
                        vulnerable = false;
                    }
                }
            }
        }
        
    }


    public void HiddenClaw(int target)
    {
        attacking = true;
        //ready = true;
        loadingattack = 1;
        aggro = team[target];
        targetAggro = target;
        StartCoroutine(Claw());
    }

    //Le coup de griffe
    IEnumerator Clawing()
    {
        sr.color = Color.white;
        //On enlève le mode attaque
        attacking = false;


        Vector2 targeting = aggro.transform.position - transform.position;
        float angle = Mathf.Atan2(targeting.y, targeting.x) * Mathf.Rad2Deg;

        //Et on met les deux slashs. Ils font 120°, tournés à 30°, de manière à faire un demi cercle où le milieu est touché deux fois.
        GameObject temp = Instantiate(claw, transform.position, Quaternion.identity);
        temp.transform.rotation = Quaternion.Euler(0, 0, angle);
        temp.transform.Rotate(new Vector3(0, 0, -25));
        temp.transform.localScale = transform.localScale*temp.transform.localScale.x;

        yield return new WaitForSeconds(0.2f);
        GameObject temp2 = Instantiate(claw, transform.position, Quaternion.identity);
        temp2.transform.localScale = transform.localScale*temp2.transform.localScale.x;

        temp2.transform.rotation = Quaternion.Euler(0, 0, angle);
        temp2.transform.Rotate(new Vector3(0, 0, -145));
        if (hidden)
        {
            sr.color = Color.clear;
        }
        
    }

    public void Kamaitachi()
    {
        //On enlève le mode attaque
        attacking = false;


        Vector2 targeting = aggro.transform.position - transform.position;
        float angle = Mathf.Atan2(targeting.y, targeting.x) * Mathf.Rad2Deg;
        angle -= 90;
        GameObject temp = Instantiate(kama, transform.position, Quaternion.identity);
        temp.transform.localScale = temp.transform.localScale*transform.localScale.x;
        temp.transform.rotation = Quaternion.Euler(0, 0, angle+90);

        temp.transform.Translate((Vector3.right * 2), Space.Self);
        sr.color = Color.white;
    }




    public void BecomeVulnerable()
    {
        box.enabled = true;
        vulnerable = true;
        StartCoroutine(Vulnerabilite());
        for(int i=0; i < weaks.Length; i++)
        {
            weaks[i].SetActive(false);
        }

    }


    void RemoveHide()
    {
        can.gameObject.SetActive(true);

        sr.color = Color.white;
        detection.SetActive(false);
        stopped = false;
        hiddenThreshold = maxHiddenThreshold;
        hidden = false;

        for (int i = 0; i < weaks.Length; i++)
        {
            weaks[i].SetActive(true);
            weaks[i].transform.localPosition = pos[i];
            if (sr.flipX)
            {
                weaks[i].transform.localPosition = new Vector3(-pos[i].x, pos[i].y, pos[i].z);
            }
        }
            

        box.enabled = false;
    }


    public void Hide()
    {

        if (!stunned)
        {
            box.enabled = true;
            sr.color = Color.clear;
            detection.SetActive(true);
            int rand = Random.Range(0, flowers.Count);
            transform.position = flowers[rand].position;
            hidden = true;
            StopCoroutine(Attack());
            attacking = false;
            ready = false;
            can.gameObject.SetActive(false);
        }
        
    }

    IEnumerator Crystal()
    {
        stopped = true;
        Vector2 targeting = aggro.transform.position - transform.position;
        float angle = Mathf.Atan2(targeting.y, targeting.x) * Mathf.Rad2Deg;
        Vector3 pos = transform.position;
        //angle -= 90;
        
        crystalin = true;
        for(int i=0; i<5; i++)
        {
            GameObject temp = Instantiate(crystalSol, pos, Quaternion.identity);
            temp.transform.localScale = transform.localScale;

            temp.transform.rotation = Quaternion.Euler(0, 0, angle + 90 + 72*i);
            temp.transform.Translate((Vector3.up * 2), Space.Self);
            temp.transform.rotation = Quaternion.identity;
        }
        yield return new WaitForSeconds(0.5f);
        for (int i = 0; i < 5; i++)
        {
            GameObject temp = Instantiate(crystalSol, pos, Quaternion.identity);
            temp.transform.localScale = transform.localScale;

            temp.transform.rotation = Quaternion.Euler(0, 0, angle + 45 + 72 * i);
            temp.transform.Translate((Vector3.up * 4), Space.Self);
            temp.transform.rotation = Quaternion.identity;
        }
        yield return new WaitForSeconds(0.5f);
        for (int i = 0; i < 10; i++)
        {
            GameObject temp = Instantiate(crystalSol, pos, Quaternion.identity);
            temp.transform.localScale = transform.localScale;

            temp.transform.rotation = Quaternion.Euler(0, 0, angle + 90 + 36 * i);
            temp.transform.Translate((Vector3.up * 6), Space.Self);
            temp.transform.rotation = Quaternion.identity;
        }

        attacking = false;
        crystalin = false;
        if (!hidden)
        {
            sr.color = Color.white;
        }
        
        timeAttack = minTimeAttack;
        stopped = false;
    }

    IEnumerator Vulnerabilite()
    {

        yield return new WaitForSeconds(vulneTime);
        vulnerable = false;
        Hide();
    }

    IEnumerator Claw()
    {
        yield return new WaitForSeconds(0.25f);
        StartCoroutine(Clawing());
    }

    IEnumerator Attack()
    {
        yield return new WaitForSeconds(0.5f);
        if (attacking)
        {
            if (loadingattack == 1)
            {

                StartCoroutine(Clawing());
                timeAttack = minTimeAttack;
            }
            else if (loadingattack == 2)
            {
                if (!crystalin)
                {
                    StartCoroutine(Crystal());
                }
            }
            else if (loadingattack == 3)
            {

                Kamaitachi();
                timeAttack = minTimeAttack;
            }
        }

        ready = false;
        stopped = false;
        
    }

    public override void EndStun()
    {
        Hide();
    }


}
