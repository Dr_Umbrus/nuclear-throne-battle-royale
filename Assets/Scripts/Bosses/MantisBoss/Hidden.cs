﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hidden : MonoBehaviour
{
    public BossMantis mantis;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "player")
        {
            mantis.HiddenClaw(collision.GetComponent<Character>().playerNum);
        }
        
    }
}
