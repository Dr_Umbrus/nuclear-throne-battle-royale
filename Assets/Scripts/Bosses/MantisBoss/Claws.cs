﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Claws : MonoBehaviour
{
    public List<GameObject> touche;
    PolygonCollider2D col;
    Animator anim;

    public int damage;
    public float prepareTime;
    public bool hit = false;
    SpriteRenderer sr;
    public Sprite death;
    float a = 1;
    public GameObject father;
    

    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        col = GetComponent<PolygonCollider2D>();
        col.enabled = false;
        StartCoroutine(Signe());
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "player")
        {
            if (touche.Count > 0)
            {
                bool potato = true;
                for (int i = 0; i < touche.Count; i++)
                {
                    if (collision.gameObject == touche[i])
                    {
                        potato = false;
                    }
                }

                if (potato)
                {
                    collision.GetComponent<Character>().Damage(damage, -1);
                    touche.Add(collision.gameObject);
                }
            }
            else
            {
                collision.GetComponent<Character>().Damage(damage, -1);
                touche.Add(collision.gameObject);
            }
            
            
        }
    }

    private void Update()
    {
        if (hit)
        {
            a -= Time.deltaTime;
            sr.color = Color.Lerp(Color.clear, Color.red,a);
        }

        if (sr.color.a < 0.2)
        {
            Destroy(father);
        }
        /*if(hit && sr.sprite == death)
        {
            Destroy(gameObject);
        }*/
    }

    IEnumerator Signe()
    {
        yield return new WaitForSeconds(prepareTime);
        sr.color = Color.red;
        hit = true;
        col.enabled = true;
        //anim.SetTrigger("damage");
        
        
    }
}
