﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrystalSol : MonoBehaviour
{
    SpriteRenderer sr;
    PolygonCollider2D col;
    public int damage=7;
    bool ok = false;

    public Sprite activate, max, deactivate, ded;

    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        col = GetComponent<PolygonCollider2D>();
        col.enabled=false;
    }

    private void Update()
    {
        if(sr.sprite==max && !ok)
        {
            StartCoroutine(Dam());
        }
        if (sr.sprite == activate)
        {
            col.enabled = true;
        }

        if (sr.sprite == deactivate)
        {
            col.enabled = false;
        }
        if (sr.sprite == ded)
        {
            StartCoroutine(Kill());
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "player")
        {

                collision.GetComponent<Character>().Damage(damage, -1);


        }
    }

    IEnumerator Kill()
    {
        yield return new WaitForSeconds(0.05f);
        Destroy(gameObject);
    }

    IEnumerator Dam()
    {
        ok = true;
        yield return new WaitForSeconds(1);
        GetComponent<Animator>().SetTrigger("Rip");
    }
}
