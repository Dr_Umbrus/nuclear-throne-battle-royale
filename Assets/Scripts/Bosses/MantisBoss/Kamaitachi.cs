﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kamaitachi : Projectile
{
    public float startSpeed;
    public float timer=0;



    public override void Pattern()
    {

            //Il va en ligne droite
            transform.Translate((Vector3.up * speed * Time.deltaTime), Space.Self);
        
        timer += Time.deltaTime*2;
        speed = startSpeed + Mathf.Pow(timer, 3);

    }

    

    public override void HitPlayer(Collider2D collision)
    {
        //Et il fait des dégâts au joueur
        collision.GetComponent<Character>().Damage(damage, player);
    }

    public override void Touch(Collider2D collision)
    {
        //Il meurt contre les murs
        StartCoroutine(Dying());

    }

}
