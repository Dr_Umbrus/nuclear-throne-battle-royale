﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss : MonoBehaviour
{
    //Classe générale de boss
    public bool stunned = false;
    public bool dead = false;
    public float timeDead = 1;

    public GameObject treasure;
    public bool roundStun;

    public bool vulnerable = false;
    public float vulneTime;

    public Slider lifebar;

    //Animator
    public Animator anim;

    //Le sprite de départ, pour vérifier l'état
    public Sprite baseSprite;

    //Un prefab pour les drops random
    public GameObject gun;

    //Hp du boss
    public float life;

    //Game Manager
    public GameManager gm;

    //Sprite renderer
    public SpriteRenderer sr;

    //Pour qu'il s'arrête quand nécessaire
    public bool stopped;

    //Les joueurs
    public GameObject[] team;

    //Le joueur à aggro
    public GameObject aggro;
    public int targetAggro;

    //La vitesse de déplacement
    public float speed;

    //Le temps restant min avant une attaque. Le temps minimal entre deux attaques.
    public float timeAttack, minTimeAttack;

    //Les phases du boss.
    public int phase=0;

    //Les limites de hp pour changer de phase
    public float[] threshold;

    //La quantité de damage pour drop des armes, le point de départ, et l'aléatoire autour de ce point
    public int damageWeapon, maxDamageWeapon, variance;

    //Le % de chances que le boss attaque quand on check
    public int chance;

    //La fréquence de check
    public float frequency;

    //Sécurités pour savoir quand il est en train d'attaquer
    public bool ready = false;
    public bool attacking = false;
    public bool loading=false;

    //Quelle attaque est en court
    public int loadingattack = -1;

    public bool started = false;

    public int[] damages;

    public AudioSource audi;

    public Color deadcol;

    public void Awake()
    {
        audi = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        anim.enabled = false;
        transform.Translate(new Vector3(0, 0, -3));
    }


    public void SetStats(GameManager gm2)
    {
        //Récupérer les composants
        sr = GetComponent<SpriteRenderer>();
        gm = gm2;
        baseSprite = sr.sprite;

        for (int i = 0; i < gm.nPlayer; i++)
        {
            team[i] = gm.team[i].gameObject;
        }

        //Décider au hasard de la première cible
        int potato = Random.Range(0, gm.nPlayer);
        aggro = team[potato];
        float potalife = gm.nPlayer;
        potalife/=4;
        life *= potalife;
        
        for (int i = 0; i < threshold.Length; i++)
        {
            threshold[i] *= potalife;
        }

        //S'il y a besoin de trucs uniques au boss
        Starting();

        started = true;
        anim.enabled = true;

        
    }

    //Lancé au démarrage
    public virtual void Starting()
    {

    }

    public void FixedUpdate()
    {
        if (stunned)
        {
            if (dead)
            {
                sr.color = deadcol;
            }
            else
            {
                sr.color = Color.yellow;
            }
            stopped = true;
            
        }
        if (started && !dead)
        {
            //Pattern unique du boss
            Pattern();


/*
            //S'il est pas stun/en train de taper
            if (!stopped)
            {
                //Il regarde sa cible
                Vector2 target = aggro.transform.position - transform.position;
                target.Normalize();

                //Puis il va vers elle
                transform.Translate(target * speed * Time.deltaTime);

                //Flip le sprite si nécessaire.
                if (target.x < 0 && !sr.flipX)
                {
                    sr.flipX = true;
                }
                if (target.x > 0 && sr.flipX)
                {
                    sr.flipX = false;
                }

                //Cooldown d'attaque
                if (timeAttack > 0)
                {
                    timeAttack -= Time.deltaTime;
                }
            }*/

            //Quand on a fini d'attaquer et qu'on est revenu au sprite de base
            if (sr.sprite == baseSprite && ready && !attacking)
            {
                //On enlève les sécurités et on lance le timer du prochain coup
                timeAttack = minTimeAttack;
                ready = false;
                attacking = false;
                loading = false;
                stopped = false;
            }
        }

        if (dead)
        {
            timeDead -= Time.deltaTime;
            sr.color = Color.Lerp(Color.clear, Color.white, timeDead);
        }
       
    }

    //Pattern du boss
    public virtual void Pattern()
    {

    }

    //Quand il prend des damages. On veut la valeur et qui les inflige.
    public virtual void Damaged(int damage, int owner)
    {
       
    }

    public void targetDeath(int targetDead)
    {
        if (targetDead == targetAggro)
        {
            

            int rand = Random.Range(0, gm.nPlayer);

            while (rand == targetAggro)
            {
                rand = Random.Range(0, gm.nPlayer);
            }

            aggro = team[rand];
        }
    }


    //Ce qui se passe à la mort. Pas encore défini.
    public IEnumerator Dying()
    {
        stunned = true;
        dead = true;
        yield return new WaitForSeconds(15);
        gm.EndGame();
    }

    //Checks pour savoir si on attaque
    public IEnumerator Load()
    {
        //On attend le temps entre deux try
        yield return new WaitForSeconds(frequency);

        //Puis on définit au hasard si on peut attaquer
        int rand = Random.Range(0, 100);
        if (rand <= chance)
        {
            //On change les sécurités
            timeAttack = minTimeAttack;
            loading = false;
            ready = true;
        }
        
        //Si on a pas réussi, on relance
        if (loading)
        {
            StartCoroutine(Load());
        }
        
    }

    public IEnumerator Stun()
    {
        ready = false;
        attacking = false;

        stunned = true;
        sr.color = Color.yellow;
        stopped = true;
        if (phase == 0)
        {
            yield return new WaitForSeconds(3);
        }
        else if(phase == threshold.Length - 1)
        {
            yield return new WaitForSeconds(12);
        }
        else
        {
            yield return new WaitForSeconds(2);
        }
        stopped = false;
        sr.color = Color.white;
        stunned = false;
        EndStun();
        
    }

    public virtual void EndStun()
    {

    }
}
