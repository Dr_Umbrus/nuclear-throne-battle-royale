﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsManager : MonoBehaviour
{
    //Le script qui gère l'UI en jeu
    public Color orange;
    bool[] big=new bool[4];
    public int[] scoresNum;
    public int[] oldScores;
    //Les canvas parents. Permet d'activer seulement l'ui des joueurs qu'on a
    public GameObject[] parentsCan;
    //le game manager
    GameManager gm;
    //Le temps restant, texte (note : En faire un vrai timer ?
    public Text timer;
    //Le temps restant
    public float time;
    //Les barres de vie, texte
    public Text[] lifes;
    //Les sliders de vie
    public Slider[] lifeSlide;

    public Text[] scores;

    //Les joueurs
    public Character[] players;
    //Les images coupées en biais ou pas (note, elles sont inversées, top c'est celles d'en bas non coupées, et inversement)
    public Sprite[] portrait;
    //Les images où mettre les portraits
    public Image[] portraits;

    public Image[] mana;

    public bool started = false;

    public float timeSummon=99;
    public float maxTimesummon;
    public GameObject smash;
    GameObject summoned = null;

    public GameObject go;


    private void Awake()
    {
        timeSummon = maxTimesummon;
        gm = FindObjectOfType<GameManager>();
    }

    //Appelé par le gm
    public void SelectPlayers(Character[] team)
    {
        for (int i = 0; i < 4; i++)
        {
            big[i] = false;
        }
        //On récupère les joueurs
        for(int i=0; i<team.Length; i++)
        {
            players[i] = team[i];
        }

        //Mise en place du temps (note : rajouter un moyen de le sélectionner avant ?)
        if (!gm.deathmatch)
        {
            time = 120;
        }
        else
        {
            time = 240;
        }
        
        for (int i = 0; i < players.Length; i++)
        {
            //Pour chaque joueur existant
            if (players[i] != null)
            {
                //On active le parent
                parentsCan[i].SetActive(true);

                //On met en place la vie
                lifeSlide[i].maxValue = players[i].maxLife;
                lifes[i].text = players[i].life.ToString() + " / " + players[i].maxLife.ToString();
                lifeSlide[i].value = players[i].life;
                //On met en place les portraits
                portraits[i].sprite = portrait[gm.chosenCharacters[i]];
                scores[i].text = "0";
                mana[i].fillAmount = 0;

            }
            
        }



        StartCoroutine(Waiting());
    }

    IEnumerator Waiting()
    {
        
        yield return new WaitForSeconds(2);
        started = true;
        //go.SetActive(true);
    }

    private void FixedUpdate()
    {
        if (started)
        {
                //On sonne la fin de partie si le timer tombe à 0
                if (time > 0)
                {
                    time -= Time.deltaTime;
                }
                else
                {
                    gm.EndGame();
                }

                //Le temps est mis à jour, sous forme d'int
                timer.text = Mathf.CeilToInt(time).ToString();
            if (Mathf.CeilToInt(time) < 4)
            {
                timer.fontSize = 120;
                timer.color = Color.Lerp(Color.red, Color.yellow, time / 4);
            }


            //Mise à jour de l'UI
            UpdateStats();

            timeSummon -= Time.deltaTime;

            if (!gm.deathmatch)
            {
                if (timeSummon <= 0 && summoned == null)
                {
                    Vector2 potato = new Vector2(transform.position.x, transform.position.y);
                    summoned = Instantiate(smash, potato + Random.insideUnitCircle * 3, Quaternion.identity);
                    summoned.GetComponent<SmashBall>().sm = this;
                }
            }
            
        }
        
    }

    public void UpdateStats()
    {
        //Reprend basiquement la création, on update la vie, le slider, les munitions, et les armes
        for(int i=0; i<players.Length; i++)
        {
            if (players[i] != null)
            {
                lifes[i].text = players[i].life.ToString() + " / " + players[i].maxLife.ToString();
                lifeSlide[i].value = players[i].life;
                scoresNum[i] = players[i].AskScore();
                if(scoresNum[i]> oldScores[i] && !big[i])
                {
                    StartCoroutine(ScoreUp(i));
                }
                scores[i].text = scoresNum[i].ToString();
                mana[i].fillAmount = 1-players[i].timerComp / players[i].maxTimerComp;
                oldScores[i] = scoresNum[i];
            }
            
        }
    }

    IEnumerator ScoreUp(int charac)
    {
        big[charac] = true;
        int taille = scores[charac].fontSize;
        scores[charac].color= Color.yellow;
        for(int i= 0; i< scores[charac].fontSize /4; i++)
        {
            scores[charac].fontSize+=2;
            yield return 0;
        }

        bool size = true;

        for (int i = 0; i < gm.nPlayer; i++)
        {
            if (scoresNum[charac] < scoresNum[i])
            {
                size = false;
            }
        }

        if (size)
        {
            scores[charac].fontSize = Mathf.CeilToInt(scores[charac].fontSize*1.5f);
            scores[charac].color = Color.red;
            yield return new WaitForSeconds(0.2f);
            int removeSize = Mathf.CeilToInt((scores[charac].fontSize - taille) / 3);
            scores[charac].fontSize = scores[charac].fontSize - removeSize;
            scores[charac].color = orange;
            yield return new WaitForSeconds(0.15f);
            scores[charac].fontSize = scores[charac].fontSize - removeSize;
            scores[charac].color = Color.yellow;
            yield return new WaitForSeconds(0.1f);
            scores[charac].fontSize = scores[charac].fontSize - removeSize;
        }
        else
        {
            yield return new WaitForSeconds(0.1f);
            scores[charac].fontSize = taille;
        }

        scores[charac].color = Color.black;

        big[charac] = false;
    }

    public void EndSmash()
    {
        timeSummon = maxTimesummon;
        summoned = null;
        
    }
}
