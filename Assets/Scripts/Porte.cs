﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Porte : MonoBehaviour
{
    GameManager gm;
    public Porte[] portes;
    public bool ok = false;
    bool checking = false;
    //public GameObject wall;
    public int id;
    BoxCollider2D box;

    private void Awake()
    {
        box = GetComponent<BoxCollider2D>();
        gm = FindObjectOfType<GameManager>();
        if (id > gm.nPlayer)
        {
            ok = true;
            //swall.SetActive(true);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!checking)
        {
            if (collision.gameObject.tag == "player")
            {

                ok = true;
                bool check = true;
                for (int i = 0; i < portes.Length; i++)
                {
                    if (!portes[i].ok)
                    {
                        check = false;
                    }
                }
                if (check)
                {
                    gm.AddBoss();
                    foreach (Porte por in portes)
                    {
                        por.CreateWall();
                    }
                    CreateWall();
                }

            }
        }
        
    }

    public void CreateWall()
    {
        checking = true;
        //wall.SetActive(true);
        StartCoroutine(Die());

    }

    IEnumerator Die()
    {
        yield return 0;
        box.enabled = false;
    }
}
