// GENERATED AUTOMATICALLY FROM 'Assets/Actions/NuclearThroneBattleRoyale.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @NuclearThroneBattleRoyale : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @NuclearThroneBattleRoyale()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""NuclearThroneBattleRoyale"",
    ""maps"": [
        {
            ""name"": ""PlayerCon"",
            ""id"": ""d9873699-d81e-41e6-ba85-4204a451848c"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""PassThrough"",
                    ""id"": ""ad55b621-88b2-46c6-a919-ac67746e0b91"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Look"",
                    ""type"": ""Value"",
                    ""id"": ""345cfc50-5df4-471f-b76a-9f80db009811"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Fire"",
                    ""type"": ""Button"",
                    ""id"": ""39b1cd80-e62f-4549-9f10-0a3257f0e23b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Fire1"",
                    ""type"": ""Button"",
                    ""id"": ""cda3350b-e3e1-46eb-a28b-cb7da45ecd5a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=1)""
                },
                {
                    ""name"": ""Grab"",
                    ""type"": ""Button"",
                    ""id"": ""5fb4de15-131b-40eb-b61f-81190fb2d7b8"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Spell"",
                    ""type"": ""Button"",
                    ""id"": ""853fba1f-cefa-4ccf-b8e4-cfd5ab49f6d1"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""EndSpell"",
                    ""type"": ""Button"",
                    ""id"": ""e7812754-706c-4501-9aa0-1afa6b41b77a"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=1)""
                },
                {
                    ""name"": ""Pause"",
                    ""type"": ""Button"",
                    ""id"": ""0067ba75-6f54-4442-85f6-4ae4b77061e8"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=1)""
                },
                {
                    ""name"": ""Back"",
                    ""type"": ""Button"",
                    ""id"": ""f6722d2c-2326-4ec0-8313-ffbc1552de90"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""978bfe49-cc26-4a3d-ab7b-7d7a29327403"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";Gamepad"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2cae3d0b-96a2-4033-b905-350d40ab4ad6"",
                    ""path"": ""<SwitchProControllerHID>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Switch"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1635d3fe-58b6-4ba9-a4e2-f4b964f6b5c8"",
                    ""path"": ""<XRController>/{Primary2DAxis}"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XR"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""685ee14f-a09b-409d-924f-a9effc386fc1"",
                    ""path"": ""<XInputController>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""X360"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c1f7a91b-d0fd-4a62-997e-7fb9b69bf235"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";Gamepad"",
                    ""action"": ""Look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""863dbe7a-5072-428c-a483-8bccefaf4042"",
                    ""path"": ""<SwitchProControllerHID>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Switch"",
                    ""action"": ""Look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3e5f5442-8668-4b27-a940-df99bad7e831"",
                    ""path"": ""<Joystick>/{Hatswitch}"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Joystick"",
                    ""action"": ""Look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""aad89e86-ec72-4925-ba86-9819c9817d7d"",
                    ""path"": ""<XInputController>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""X360"",
                    ""action"": ""Look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""fd424b33-dc87-4d73-82a4-a6ba2e68c81c"",
                    ""path"": ""<SwitchProControllerHID>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Switch"",
                    ""action"": ""Fire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ee3d0cd2-254e-47a7-a8cb-bc94d9658c54"",
                    ""path"": ""<Joystick>/trigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Joystick"",
                    ""action"": ""Fire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8255d333-5683-4943-a58a-ccb207ff1dce"",
                    ""path"": ""<XRController>/{PrimaryAction}"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XR"",
                    ""action"": ""Fire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""886e731e-7071-4ae4-95c0-e61739dad6fd"",
                    ""path"": ""<Touchscreen>/primaryTouch/tap"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";Touch"",
                    ""action"": ""Fire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9aae7977-6dbd-4b4f-a523-9bd995370e04"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Fire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""60146b73-ecff-4aa6-b8b7-aca97a056b19"",
                    ""path"": ""<XInputController>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""X360"",
                    ""action"": ""Fire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4bb3a7bd-0219-4d4b-a7c1-d94f4940168b"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Grab"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""802bd57a-8de9-4706-972b-870ae693a04d"",
                    ""path"": ""<SwitchProControllerHID>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Switch"",
                    ""action"": ""Grab"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3b81e5d5-5590-456d-98c1-867bb8df03e2"",
                    ""path"": ""<XInputController>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""X360"",
                    ""action"": ""Grab"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9f2224c6-3cae-42c0-9961-8bab413c7227"",
                    ""path"": ""<XInputController>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""X360"",
                    ""action"": ""Spell"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""850da02a-577e-46c5-8804-7119c4cbde80"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Spell"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""02416ab7-9849-4998-b3d9-face5f689fe2"",
                    ""path"": ""<SwitchProControllerHID>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Switch"",
                    ""action"": ""Spell"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9f848c9e-40cf-420e-85ea-bce2ceda1c11"",
                    ""path"": ""<XInputController>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""EndSpell"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""673bb839-ca24-4e5b-9daa-ba65d9a1f274"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""EndSpell"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""767ca447-1efc-471a-8fc1-92e3d0e1bb32"",
                    ""path"": ""<SwitchProControllerHID>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Switch"",
                    ""action"": ""EndSpell"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""286d5e50-b2cc-46d6-bf44-a1722216d7c0"",
                    ""path"": ""<XInputController>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""X360"",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""11c7b1a5-43d2-44df-b251-3bc9d52db920"",
                    ""path"": ""<SwitchProControllerHID>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Switch"",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""cfffa513-e7b5-44ee-8e5a-3b7518c10738"",
                    ""path"": ""<Gamepad>/startButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c92ac11e-555c-4f13-a517-1b62d7045168"",
                    ""path"": ""<XInputController>/select"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""99c32b70-3d68-4f2e-9b2f-1f84a57a8c78"",
                    ""path"": ""<SwitchProControllerHID>/select"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9a335eee-c30c-48a7-b0d9-82e38706c491"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Back"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3072711d-e7db-40f9-80b9-0b6958beee37"",
                    ""path"": ""<XInputController>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""X360"",
                    ""action"": ""Back"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3eda855e-553e-42e4-b0bc-edfdc27df878"",
                    ""path"": ""<SwitchProControllerHID>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Switch"",
                    ""action"": ""Back"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0d2c497c-9470-4926-8da6-35309175e8fc"",
                    ""path"": ""<SwitchProControllerHID>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Switch"",
                    ""action"": ""Fire1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""98f9aa0b-a902-4da9-ab46-657532792ca4"",
                    ""path"": ""<Joystick>/trigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Joystick"",
                    ""action"": ""Fire1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3751dc41-7e3a-4c3f-8f74-d0ec94e9a9ee"",
                    ""path"": ""<XRController>/{PrimaryAction}"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XR"",
                    ""action"": ""Fire1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c3571a85-575f-443b-8e7d-fb6afea6da1b"",
                    ""path"": ""<Touchscreen>/primaryTouch/tap"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";Touch"",
                    ""action"": ""Fire1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6ff69e9e-7712-4dd4-8303-b625cc569d43"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Fire1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e196107a-7ac3-4723-994b-f5cb5f76408e"",
                    ""path"": ""<XInputController>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""X360"",
                    ""action"": ""Fire1"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Gamepad"",
            ""bindingGroup"": ""Gamepad"",
            ""devices"": [
                {
                    ""devicePath"": ""<Gamepad>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Touch"",
            ""bindingGroup"": ""Touch"",
            ""devices"": [
                {
                    ""devicePath"": ""<Touchscreen>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Joystick"",
            ""bindingGroup"": ""Joystick"",
            ""devices"": [
                {
                    ""devicePath"": ""<Joystick>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""XR"",
            ""bindingGroup"": ""XR"",
            ""devices"": [
                {
                    ""devicePath"": ""<XRController>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Switch"",
            ""bindingGroup"": ""Switch"",
            ""devices"": [
                {
                    ""devicePath"": ""<SwitchProControllerHID>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""X360"",
            ""bindingGroup"": ""X360"",
            ""devices"": [
                {
                    ""devicePath"": ""<XInputController>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // PlayerCon
        m_PlayerCon = asset.FindActionMap("PlayerCon", throwIfNotFound: true);
        m_PlayerCon_Move = m_PlayerCon.FindAction("Move", throwIfNotFound: true);
        m_PlayerCon_Look = m_PlayerCon.FindAction("Look", throwIfNotFound: true);
        m_PlayerCon_Fire = m_PlayerCon.FindAction("Fire", throwIfNotFound: true);
        m_PlayerCon_Fire1 = m_PlayerCon.FindAction("Fire1", throwIfNotFound: true);
        m_PlayerCon_Grab = m_PlayerCon.FindAction("Grab", throwIfNotFound: true);
        m_PlayerCon_Spell = m_PlayerCon.FindAction("Spell", throwIfNotFound: true);
        m_PlayerCon_EndSpell = m_PlayerCon.FindAction("EndSpell", throwIfNotFound: true);
        m_PlayerCon_Pause = m_PlayerCon.FindAction("Pause", throwIfNotFound: true);
        m_PlayerCon_Back = m_PlayerCon.FindAction("Back", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // PlayerCon
    private readonly InputActionMap m_PlayerCon;
    private IPlayerConActions m_PlayerConActionsCallbackInterface;
    private readonly InputAction m_PlayerCon_Move;
    private readonly InputAction m_PlayerCon_Look;
    private readonly InputAction m_PlayerCon_Fire;
    private readonly InputAction m_PlayerCon_Fire1;
    private readonly InputAction m_PlayerCon_Grab;
    private readonly InputAction m_PlayerCon_Spell;
    private readonly InputAction m_PlayerCon_EndSpell;
    private readonly InputAction m_PlayerCon_Pause;
    private readonly InputAction m_PlayerCon_Back;
    public struct PlayerConActions
    {
        private @NuclearThroneBattleRoyale m_Wrapper;
        public PlayerConActions(@NuclearThroneBattleRoyale wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_PlayerCon_Move;
        public InputAction @Look => m_Wrapper.m_PlayerCon_Look;
        public InputAction @Fire => m_Wrapper.m_PlayerCon_Fire;
        public InputAction @Fire1 => m_Wrapper.m_PlayerCon_Fire1;
        public InputAction @Grab => m_Wrapper.m_PlayerCon_Grab;
        public InputAction @Spell => m_Wrapper.m_PlayerCon_Spell;
        public InputAction @EndSpell => m_Wrapper.m_PlayerCon_EndSpell;
        public InputAction @Pause => m_Wrapper.m_PlayerCon_Pause;
        public InputAction @Back => m_Wrapper.m_PlayerCon_Back;
        public InputActionMap Get() { return m_Wrapper.m_PlayerCon; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerConActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerConActions instance)
        {
            if (m_Wrapper.m_PlayerConActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_PlayerConActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_PlayerConActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_PlayerConActionsCallbackInterface.OnMove;
                @Look.started -= m_Wrapper.m_PlayerConActionsCallbackInterface.OnLook;
                @Look.performed -= m_Wrapper.m_PlayerConActionsCallbackInterface.OnLook;
                @Look.canceled -= m_Wrapper.m_PlayerConActionsCallbackInterface.OnLook;
                @Fire.started -= m_Wrapper.m_PlayerConActionsCallbackInterface.OnFire;
                @Fire.performed -= m_Wrapper.m_PlayerConActionsCallbackInterface.OnFire;
                @Fire.canceled -= m_Wrapper.m_PlayerConActionsCallbackInterface.OnFire;
                @Fire1.started -= m_Wrapper.m_PlayerConActionsCallbackInterface.OnFire1;
                @Fire1.performed -= m_Wrapper.m_PlayerConActionsCallbackInterface.OnFire1;
                @Fire1.canceled -= m_Wrapper.m_PlayerConActionsCallbackInterface.OnFire1;
                @Grab.started -= m_Wrapper.m_PlayerConActionsCallbackInterface.OnGrab;
                @Grab.performed -= m_Wrapper.m_PlayerConActionsCallbackInterface.OnGrab;
                @Grab.canceled -= m_Wrapper.m_PlayerConActionsCallbackInterface.OnGrab;
                @Spell.started -= m_Wrapper.m_PlayerConActionsCallbackInterface.OnSpell;
                @Spell.performed -= m_Wrapper.m_PlayerConActionsCallbackInterface.OnSpell;
                @Spell.canceled -= m_Wrapper.m_PlayerConActionsCallbackInterface.OnSpell;
                @EndSpell.started -= m_Wrapper.m_PlayerConActionsCallbackInterface.OnEndSpell;
                @EndSpell.performed -= m_Wrapper.m_PlayerConActionsCallbackInterface.OnEndSpell;
                @EndSpell.canceled -= m_Wrapper.m_PlayerConActionsCallbackInterface.OnEndSpell;
                @Pause.started -= m_Wrapper.m_PlayerConActionsCallbackInterface.OnPause;
                @Pause.performed -= m_Wrapper.m_PlayerConActionsCallbackInterface.OnPause;
                @Pause.canceled -= m_Wrapper.m_PlayerConActionsCallbackInterface.OnPause;
                @Back.started -= m_Wrapper.m_PlayerConActionsCallbackInterface.OnBack;
                @Back.performed -= m_Wrapper.m_PlayerConActionsCallbackInterface.OnBack;
                @Back.canceled -= m_Wrapper.m_PlayerConActionsCallbackInterface.OnBack;
            }
            m_Wrapper.m_PlayerConActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @Look.started += instance.OnLook;
                @Look.performed += instance.OnLook;
                @Look.canceled += instance.OnLook;
                @Fire.started += instance.OnFire;
                @Fire.performed += instance.OnFire;
                @Fire.canceled += instance.OnFire;
                @Fire1.started += instance.OnFire1;
                @Fire1.performed += instance.OnFire1;
                @Fire1.canceled += instance.OnFire1;
                @Grab.started += instance.OnGrab;
                @Grab.performed += instance.OnGrab;
                @Grab.canceled += instance.OnGrab;
                @Spell.started += instance.OnSpell;
                @Spell.performed += instance.OnSpell;
                @Spell.canceled += instance.OnSpell;
                @EndSpell.started += instance.OnEndSpell;
                @EndSpell.performed += instance.OnEndSpell;
                @EndSpell.canceled += instance.OnEndSpell;
                @Pause.started += instance.OnPause;
                @Pause.performed += instance.OnPause;
                @Pause.canceled += instance.OnPause;
                @Back.started += instance.OnBack;
                @Back.performed += instance.OnBack;
                @Back.canceled += instance.OnBack;
            }
        }
    }
    public PlayerConActions @PlayerCon => new PlayerConActions(this);
    private int m_GamepadSchemeIndex = -1;
    public InputControlScheme GamepadScheme
    {
        get
        {
            if (m_GamepadSchemeIndex == -1) m_GamepadSchemeIndex = asset.FindControlSchemeIndex("Gamepad");
            return asset.controlSchemes[m_GamepadSchemeIndex];
        }
    }
    private int m_TouchSchemeIndex = -1;
    public InputControlScheme TouchScheme
    {
        get
        {
            if (m_TouchSchemeIndex == -1) m_TouchSchemeIndex = asset.FindControlSchemeIndex("Touch");
            return asset.controlSchemes[m_TouchSchemeIndex];
        }
    }
    private int m_JoystickSchemeIndex = -1;
    public InputControlScheme JoystickScheme
    {
        get
        {
            if (m_JoystickSchemeIndex == -1) m_JoystickSchemeIndex = asset.FindControlSchemeIndex("Joystick");
            return asset.controlSchemes[m_JoystickSchemeIndex];
        }
    }
    private int m_XRSchemeIndex = -1;
    public InputControlScheme XRScheme
    {
        get
        {
            if (m_XRSchemeIndex == -1) m_XRSchemeIndex = asset.FindControlSchemeIndex("XR");
            return asset.controlSchemes[m_XRSchemeIndex];
        }
    }
    private int m_SwitchSchemeIndex = -1;
    public InputControlScheme SwitchScheme
    {
        get
        {
            if (m_SwitchSchemeIndex == -1) m_SwitchSchemeIndex = asset.FindControlSchemeIndex("Switch");
            return asset.controlSchemes[m_SwitchSchemeIndex];
        }
    }
    private int m_X360SchemeIndex = -1;
    public InputControlScheme X360Scheme
    {
        get
        {
            if (m_X360SchemeIndex == -1) m_X360SchemeIndex = asset.FindControlSchemeIndex("X360");
            return asset.controlSchemes[m_X360SchemeIndex];
        }
    }
    public interface IPlayerConActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnLook(InputAction.CallbackContext context);
        void OnFire(InputAction.CallbackContext context);
        void OnFire1(InputAction.CallbackContext context);
        void OnGrab(InputAction.CallbackContext context);
        void OnSpell(InputAction.CallbackContext context);
        void OnEndSpell(InputAction.CallbackContext context);
        void OnPause(InputAction.CallbackContext context);
        void OnBack(InputAction.CallbackContext context);
    }
}
